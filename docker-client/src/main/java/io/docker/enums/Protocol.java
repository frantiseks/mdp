/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.docker.enums;

/**
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class Protocol {

    public static final String TCP = "http";
    public static final String UNIX = "unix";
}
