/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.docker.exception.DockerException;
import io.docker.exception.DockerNotFoundException;
import io.docker.exception.DockerNotRunningException;
import io.docker.model.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;

/**
 * @author Frantisek Spacek
 */
public class DockerDefaultClient implements DockerClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(DockerDefaultClient.class);

    private static final String CONTAINERS_LIST = "/containers/json";
    private static final String INSPECT_CONTAINER = "/containers/%s/json";
    private static final String CREATE_CONTAINER = "/containers/create";
    private static final String START_CONTAINER = "/containers/%s/start";
    private static final String STOP_CONTAINER = "/containers/%s/stop";
    private static final String KILL_CONTAINER = "/containers/%s/kill";
    private static final String DELETE_CONTAINER = "/containers/%s?force=true";
    private static final String RESTART_CONTAINER = "/containers/%s/restart";
    private static final String GET_DOCKER_VERSION = "/version";
    private static final String GET_DOCKER_INFO = "/info";
    private static final String CONTAINERS_PROCESSES = "/containers/%s/top?ps_args=%s";
    private static final String CONTAINER_LOGS = "/containers/%s/logs?stdout=%s&stderr=%s&follow=%s&tail=%s";
    private static final String CONTAINER_ATTACH = "/containers/%s/attach?stream=0&stdout=%s&stderr=%s";
    private final String host;
    private final String protocol;
    private final int port;

    private HttpClient httpClient;

    public DockerDefaultClient() {
        this("http", "localhost", 4243);
    }

    public DockerDefaultClient(String host, int port) {
        this("http", host, port);
    }

    public DockerDefaultClient(String protocol, String host, int port) {
        this.protocol = protocol;
        this.host = host;
        this.port = port;

        if (protocol.equals("unix")) {
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("unix", new UnixSocketFactory(new File(host))).build();

            this.httpClient = HttpClients
                    .custom()
                    .setConnectionManager(new PoolingHttpClientConnectionManager(registry))
                    .build();
        } else {
            this.httpClient = HttpClients.createDefault();
        }
    }

    public String getProtocol() {
        return protocol;
    }

    @Override
    public List<Container> get(boolean all)
            throws DockerException, DockerNotRunningException {
        return get(all, false, -1, false, null, null);
    }

    @Override
    public List<Container> get(boolean all, boolean latest)
            throws DockerException, DockerNotRunningException {
        return get(all, latest, -1, false, null, null);
    }

    @Override
    public List<Container> get(boolean all, boolean latest, int limit)
            throws DockerException, DockerNotRunningException {
        return get(all, latest, limit, false, null, null);
    }

    @Override
    public List<Container> get(boolean all, boolean latest, int limit, boolean showSize)
            throws DockerException, DockerNotRunningException {
        return get(all, latest, limit, showSize, null, null);
    }

    @Override
    public List<Container> get(boolean all, boolean latest, int limit,
            boolean showSize, String since)
            throws DockerException, DockerNotRunningException {
        return get(all, latest, limit, false, since, null);
    }

    @Override
    public List<Container> get(boolean all, boolean latest,
            int limit, boolean showSize, String since, String before)
            throws DockerException, DockerNotRunningException {

        Type type = new TypeToken<Collection<Container>>() {
        }.getType();

        List<Container> result;
        try {
            URI uri = dockerBaseURI(CONTAINERS_LIST)
                    .setParameter("all", String.valueOf(all))
                    .setParameter("limit", String.valueOf(limit))
                    .setParameter("since", since != null ? String.valueOf(since) : "")
                    .setParameter("before", before != null ? String.valueOf(before) : "")
                    .setParameter("size", String.valueOf(showSize))
                    .build();

            HttpGet httpGet = new HttpGet(uri);
            LOGGER.debug("Request URI: ", httpGet.getURI());
            HttpResponse response = httpClient.execute(httpGet);

            switch (response.getStatusLine().getStatusCode()) {
                case 200:
                    LOGGER.debug("Container list succesfully retrieved");
                    break;
                case 400:
                    LOGGER.error("Bad request parameter");
                    throw new DockerException("Bad request parameter");
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }

            result = new Gson().fromJson(EntityUtils.toString(response.getEntity()), type);

        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }

        LOGGER.debug(String.format("Response: %s", result));
        return result;

    }

    @Override
    public ContainerDetail inspect(String id) throws DockerException, DockerNotRunningException {
        ContainerDetail result = null;
        try {
            URI uri = dockerBaseURI(String.format(INSPECT_CONTAINER, id)).build();
            HttpGet httpGet = new HttpGet(uri);
            LOGGER.debug("Request URI: ", httpGet.getURI());

            HttpResponse response = httpClient.execute(httpGet);

            switch (response.getStatusLine().getStatusCode()) {
                case 200:
                    LOGGER.debug(String.format("Container %s successfully inspected", id));
                    break;
                case 404:
                    LOGGER.error(String.format("Container %s not found", id));
                    throw new DockerNotFoundException(String.format("No such container %s", id));
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }

            result = new Gson().fromJson(EntityUtils.toString(response.getEntity()), ContainerDetail.class);

        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }

        LOGGER.debug(String.format("Response: %s", result));
        return result;

    }

    @Override
    public ContainerInfo create(ContainerConfig config) throws DockerException, DockerNotRunningException {
        ContainerInfo result;
        try {
            URI uri = dockerBaseURI(CREATE_CONTAINER).build();
            HttpPost httpPost = new HttpPost(uri);
            LOGGER.debug("Request URI: {}", httpPost.getURI());

            httpPost.addHeader("Content-Type", ContentType.APPLICATION_JSON.toString());

            String payload = new Gson().toJson(config);
            LOGGER.debug("Request payload: {}", payload);

            HttpEntity entity = new StringEntity(payload);
            httpPost.setEntity(entity);

            HttpResponse response = httpClient.execute(httpPost);
            switch (response.getStatusLine().getStatusCode()) {
                case 201:
                    LOGGER.debug("Container successfully created");
                    break;
                case 404:
                    LOGGER.error("Container not found");
                    throw new DockerNotFoundException("No such container");
                case 406:
                    LOGGER.error("Unable to attach to container, container not running");
                    throw new DockerException("Unable to attach to container");
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }
            result = new Gson().fromJson(EntityUtils.toString(response.getEntity()), ContainerInfo.class);

        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }
        LOGGER.debug(String.format("Response: %s", result));
        return result;
    }

    @Override
    public void start(String id, StartConfig config) throws DockerException, DockerNotRunningException {
        try {
            URI uri = dockerBaseURI(String.format(START_CONTAINER, id)).build();
            HttpPost httpPost = new HttpPost(uri);
            httpPost.addHeader("Content-Type", ContentType.APPLICATION_JSON.toString());

            LOGGER.debug("Request URI: ", httpPost.getURI());

            httpPost.addHeader("Content-Type", ContentType.APPLICATION_JSON.toString());
            HttpEntity entity = new StringEntity(new Gson().toJson(config));
            httpPost.setEntity(entity);

            HttpResponse response = httpClient.execute(httpPost);

            switch (response.getStatusLine().getStatusCode()) {
                case 204:
                    LOGGER.debug(String.format("Container %s successfully started", id));
                    break;
                case 404:
                    LOGGER.error(String.format("Container %s not found", id));
                    throw new DockerNotFoundException(String.format("No such container %s", id));
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }
        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }
    }

    @Override
    public void stop(String id) throws DockerException, DockerNotRunningException {
        try {
            URI uri = dockerBaseURI(String.format(STOP_CONTAINER, id)).build();
            HttpPost httpPost = new HttpPost(uri);
            httpPost.addHeader("Content-Type", ContentType.APPLICATION_JSON.toString());

            LOGGER.debug("Request URI: ", httpPost.getURI());

            HttpResponse response = httpClient.execute(httpPost);

            switch (response.getStatusLine().getStatusCode()) {
                case 204:
                    LOGGER.debug(String.format("Container %s successfully stopped", id));
                    break;
                case 404:
                    LOGGER.error(String.format("Container %s not found", id));
                    throw new DockerNotFoundException(String.format("No such container %s", id));
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }
        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }
    }

    @Override
    public void delete(String id) throws DockerException, DockerNotRunningException {
        try {
            URI uri = dockerBaseURI(String.format(DELETE_CONTAINER, id)).build();
            HttpDelete httpDelete = new HttpDelete(uri);
            LOGGER.debug("Request URI: ", httpDelete.getURI());
            httpDelete.addHeader("Content-Type", ContentType.APPLICATION_JSON.toString());

            HttpResponse response = httpClient.execute(httpDelete);

            switch (response.getStatusLine().getStatusCode()) {
                case 204:
                    LOGGER.debug(String.format("Container %s successfully deleted", id));
                    break;
                case 404:
                    LOGGER.error(String.format("Container %s not found", id));
                    throw new DockerNotFoundException(String.format("No such container %s", id));
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }
        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }

    }

    @Override
    public void restart(String id) throws DockerException, DockerNotRunningException {
        try {
            URI uri = dockerBaseURI(String.format(RESTART_CONTAINER, id)).build();
            HttpPost httpPost = new HttpPost(uri);
            LOGGER.debug("Request URI: ", httpPost.getURI());
            httpPost.addHeader("Content-Type", ContentType.APPLICATION_JSON.toString());

            HttpResponse response = httpClient.execute(httpPost);

            switch (response.getStatusLine().getStatusCode()) {
                case 204:
                    LOGGER.debug(String.format("Container %s successfully restarted", id));
                    break;
                case 404:
                    LOGGER.error(String.format("Container %s not found", id));
                    throw new DockerNotFoundException(String.format("No such container %s", id));
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }
        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }

    }

    @Override
    public void kill(String id) throws DockerException, DockerNotRunningException {
        try {
            URI uri = dockerBaseURI(String.format(KILL_CONTAINER, id)).build();
            HttpPost httpPost = new HttpPost(uri);
            httpPost.addHeader("Content-Type", ContentType.APPLICATION_JSON.toString());

            LOGGER.debug("Request URI: ", httpPost.getURI());

            HttpResponse response = httpClient.execute(httpPost);

            switch (response.getStatusLine().getStatusCode()) {
                case 204:
                    LOGGER.debug(String.format("Container %s successfully killed", id));
                    break;
                case 404:
                    LOGGER.error(String.format("Container %s not found", id));
                    throw new DockerNotFoundException(String.format("No such container %s", id));
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }
        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }
    }

    @Override
    public DockerVersion getVersion() throws DockerException, DockerNotRunningException {
        DockerVersion result;
        try {
            URI uri = dockerBaseURI(GET_DOCKER_VERSION).build();
            HttpGet httpGet = new HttpGet(uri);
            LOGGER.debug("Request URI: ", httpGet.getURI());

            HttpResponse response = httpClient.execute(httpGet);

            switch (response.getStatusLine().getStatusCode()) {
                case 200:
                    LOGGER.debug(String.format("No error"));
                    break;
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }

            result = new Gson().fromJson(EntityUtils.toString(response.getEntity()), DockerVersion.class);

        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }

        LOGGER.debug(String.format("Response: %s", result));
        return result;
    }

    @Override
    public DockerInfo getInfo() throws DockerException, DockerNotRunningException {
        DockerInfo result = null;
        try {
            URI uri = dockerBaseURI(GET_DOCKER_INFO).build();
            HttpGet httpGet = new HttpGet(uri);
            LOGGER.debug("Request URI: ", httpGet.getURI());

            HttpResponse response = httpClient.execute(httpGet);

            switch (response.getStatusLine().getStatusCode()) {
                case 200:
                    LOGGER.debug(String.format("No error"));
                    break;
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }

            result = new Gson().fromJson(EntityUtils.toString(response.getEntity()), DockerInfo.class);

        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }

        LOGGER.debug(String.format("Response: %s", result));
        return result;
    }

    private URIBuilder dockerBaseURI(String path) throws URISyntaxException {
        if (protocol.equals("unix")) {
            return new URIBuilder(String.format("unix://localhost:%s/%s", port, path));
        } else {
            return new URIBuilder(String.format("%s://%s:%s%s", protocol, host, port, path));
        }
    }

    @Override
    public ContainerProcesses getProcesses(String id, String psArgs) throws DockerException, DockerNotRunningException {
        ContainerProcesses result = null;
        try {
            URI uri = dockerBaseURI(String.format(CONTAINERS_PROCESSES, id, psArgs)).build();
            HttpGet httpGet = new HttpGet(uri);
            LOGGER.debug("Request URI: ", httpGet.getURI());

            HttpResponse response = httpClient.execute(httpGet);

            switch (response.getStatusLine().getStatusCode()) {
                case 200:
                    LOGGER.debug(String.format("No error"));
                    break;
                case 404:
                    LOGGER.error(String.format("Container %s not found", id));
                    throw new DockerNotFoundException(String.format("No such container %s", id));
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }

            result = new Gson().fromJson(EntityUtils.toString(response.getEntity()), ContainerProcesses.class);

        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }

        return result;
    }

    @Override
    public String getLogs(String id, boolean stdout, boolean stderr) throws DockerException, DockerNotRunningException {
        return getLogs(id, stdout, stderr, true);
    }

    @Override
    public String getLogs(String id, boolean stdout, boolean stderr, boolean follow) throws DockerException, DockerNotRunningException {
        return getLogs(id, stdout, stderr, follow, 0);
    }

    @Override
    public String getLogs(String id, boolean stdout, boolean stderr, boolean follow, int tail) throws DockerException, DockerNotRunningException {
        String result = null;
        try {
            URI uri = dockerBaseURI(String.format(CONTAINER_LOGS, id, stdout, stderr, follow, tail == 0 ? "all" : tail)).build();
            HttpGet httpGet = new HttpGet(uri);
            LOGGER.debug("Request URI: ", httpGet.getURI());

            HttpResponse response = httpClient.execute(httpGet);

            switch (response.getStatusLine().getStatusCode()) {
                case 200:
                    LOGGER.debug(String.format("No error"));
                    break;
                case 404:
                    LOGGER.error(String.format("Container %s not found", id));
                    throw new DockerNotFoundException(String.format("No such container %s", id));
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }
            result = EntityUtils.toString(response.getEntity());

        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }

        return result;
    }

    @Override
    public InputStream attach(String id, boolean stdout, boolean stderr) throws DockerException, DockerNotRunningException {
        try {
            URI uri = dockerBaseURI(String.format(CONTAINER_ATTACH, id, stdout, stderr)).build();
            HttpPost httpPost = new HttpPost(uri);
            httpPost.addHeader("Content-Type", ContentType.APPLICATION_OCTET_STREAM.toString());
            LOGGER.debug("Request URI: ", httpPost.getURI());

            HttpResponse response = httpClient.execute(httpPost);

            switch (response.getStatusLine().getStatusCode()) {
                case 200:
                    LOGGER.debug(String.format("No error"));
                    break;
                case 404:
                    LOGGER.error(String.format("Container %s not found", id));
                    throw new DockerNotFoundException(String.format("No such container %s", id));
                case 500:
                    LOGGER.error("Docker Server Error");
                    throw new DockerException("Docker Server Error");
                default:
                    throw new DockerException("Unknown Error");
            }
            return response.getEntity().getContent();

        } catch (ConnectException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerNotRunningException(ex);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            LOGGER.error("DockerClient", ex);
            throw new DockerException(ex);
        }
    }

    public static class Builder {

        private String protocol;
        private String host;
        private int port;

        public Builder withProtocol(String protocol) {
            this.protocol = protocol;
            return this;
        }

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public Builder withPort(int port) {
            this.port = port;
            return this;
        }

        public DockerDefaultClient build() {
            return new DockerDefaultClient(protocol, host, port);
        }
    }
}
