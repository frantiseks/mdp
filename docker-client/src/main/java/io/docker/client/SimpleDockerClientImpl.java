/*
 *
 */
package io.docker.client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.docker.exception.DockerErrorException;
import io.docker.exception.DockerException;
import io.docker.exception.DockerNotFoundException;
import io.docker.exception.DockerNotRunningException;
import io.docker.exception.InvalidRequestException;
import io.docker.filter.ListFilter;
import io.docker.filter.LogsFilter;
import io.docker.model.Container;
import io.docker.model.ContainerConfig;
import io.docker.model.ContainerDetail;
import io.docker.model.ContainerInfo;
import io.docker.model.StartConfig;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import static java.lang.String.format;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link SimpleDockerClient}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class SimpleDockerClientImpl implements SimpleDockerClient {

    private static final Logger LOG = LoggerFactory
            .getLogger(SimpleDockerClientImpl.class);

    private static final int HTTP_PORT = 80;

    private static final String BASE_URL_PATTERN = "unix://localhost:%s/%s";
    private static final String PING_ENDPOINT = "/_ping";
    private static final String INSPECT_ENDPOINT = "/containers/%s/json";
    private static final String CREATE_ENDPOINT = "/containers/create";
    private static final String START_ENDPOINT = "/containers/%s/start";
    private static final String STOP_ENDPOINT = "/containers/%s/stop";
    private static final String KILL_ENDPOINT = "/containers/%s/kill";
    private static final String DELETE_ENDPOINT = "/containers/%s?force=true";
    private static final String LIST_ENDPOINT = "/containers/json";
    private static final String LOGS_ENDPOINT = "/containers/%s/logs";

    private final HttpClient httpClient;

    public SimpleDockerClientImpl(String socketPath) {
        File socket = new File(socketPath);

        if (!socket.exists()) {
            throw new IllegalStateException("Specified docker engine"
                    + " socket does not exists");
        }

        Registry<ConnectionSocketFactory> registry = RegistryBuilder
                .<ConnectionSocketFactory>create()
                .register("unix", new UnixSocketFactory(socket))
                .build();

        HttpClientConnectionManager conManager
                = new PoolingHttpClientConnectionManager(registry);

        this.httpClient = HttpClients.custom()
                .setConnectionManager(conManager)
                .build();
    }

    @Override
    public void ping() throws DockerNotRunningException, DockerErrorException {
        try {
            URI uri = dockerBaseUri(PING_ENDPOINT).build();
            HttpGet httpGet = new HttpGet(uri);

            LOG.debug("Executing request {}", uri);
            HttpResponse response = httpClient.execute(httpGet);

            int statusCode = response.getStatusLine().getStatusCode();
            LOG.debug("Returned status code {}", statusCode);

            switch (statusCode) {
                case 200:
                    break;
                case 500:
                    throw new DockerErrorException("Docker Server Error");
                default:
                    throw new DockerErrorException("Unknown Error");
            }
        } catch (URISyntaxException ex) {
            throw new DockerErrorException("Unable to ping docker daemon", ex);
        } catch (IOException ex) {
            throw new DockerNotRunningException("Docker daemon is not running",
                    ex);
        }

    }

    @Override
    public List<Container> get(ListFilter filter)
            throws InvalidRequestException, DockerErrorException {
        StringBuilder filters = new StringBuilder();

        if (filter.getExitedCode() != null) {
            filters.append("exited=").append(filter.getExitedCode())
                    .append(";");
            filters.append("status=").append(filter.getStatus())
                    .append(";");
        }

        String limit = filter.getLimit() == null
                ? ""
                : String.valueOf(filter.getLimit());

        try {
            URI uri = dockerBaseUri(LIST_ENDPOINT)
                    .addParameter("all", String.valueOf(filter.getAll()))
                    .addParameter("limit", limit)
                    .addParameter("since", filter.getSinceId())
                    .addParameter("before", filter.getBeforeId())
                    .addParameter("size", String.valueOf(filter.showSize()))
                    .addParameter("filters", filters.toString())
                    .build();
            HttpGet httpGet = new HttpGet(uri);

            LOG.debug("Executing request {}", uri);
            HttpResponse response = httpClient.execute(httpGet);

            int statusCode = response.getStatusLine().getStatusCode();
            LOG.debug("Returned status code {}", statusCode);

            switch (statusCode) {
                case 200:
                    break;
                case 400:
                    throw new InvalidRequestException("Invalid request filter");
                default:
                    throw new DockerErrorException("Unknown docker error");
            }

            Type type = new TypeToken<Collection<Container>>() {
            }.getType();

            String responseJson = EntityUtils.toString(response.getEntity());
            LOG.debug("Fetched json response {}", responseJson);

            return new Gson().fromJson(responseJson, type);

        } catch (URISyntaxException | IOException ex) {
            throw new DockerErrorException("Unable to get container list", ex);
        }
    }

    @Override
    public ContainerDetail inspect(String id) throws DockerNotFoundException,
            DockerErrorException {
        try {
            URI uri = dockerBaseUri(String.format(INSPECT_ENDPOINT, id))
                    .build();
            HttpGet httpGet = new HttpGet(uri);

            LOG.debug("Executing request {}", uri);
            HttpResponse response = httpClient.execute(httpGet);

            int statusCode = response.getStatusLine().getStatusCode();
            LOG.debug("Returned status code {}", statusCode);

            switch (statusCode) {
                case 200:
                    break;
                case 404:
                    throw new DockerNotFoundException(format("No such container"
                            + " %s", id));
                default:
                    throw new DockerErrorException("Docker Server Error");
            }

            String responseJson = EntityUtils.toString(response.getEntity());
            LOG.debug("Fetched json response {}", responseJson);

            return new Gson().fromJson(responseJson, ContainerDetail.class);
        } catch (URISyntaxException | IOException ex) {
            throw new DockerErrorException("Unable to get container list", ex);
        }
    }

    @Override
    public ContainerInfo create(ContainerConfig config)
            throws InvalidRequestException, DockerErrorException {
        try {
            URI uri = dockerBaseUri(CREATE_ENDPOINT).build();

            HttpResponse response = postJsonRequest(uri, config);

            int statusCode = response.getStatusLine().getStatusCode();
            LOG.debug("Returned status code {}", statusCode);

            switch (statusCode) {
                case 201:
                    break;
                case 400:
                    throw new InvalidRequestException("Invalid container "
                            + "configuration");
                default:
                    throw new DockerException("Docker Server Error");
            }

            String responseJson = EntityUtils.toString(response.getEntity());
            LOG.debug("Fetched json response {}", responseJson);

            return new Gson().fromJson(responseJson, ContainerInfo.class);

        } catch (URISyntaxException | IOException ex) {
            throw new DockerErrorException("Unable to create container", ex);
        }
    }

    @Override
    public void start(String id, StartConfig config)
            throws DockerNotFoundException, InvalidRequestException,
            DockerErrorException {
        try {
            URI uri = dockerBaseUri(String.format(START_ENDPOINT, id)).build();
            HttpResponse response = postJsonRequest(uri, config);

            int statusCode = response.getStatusLine().getStatusCode();
            LOG.debug("Returned status code {}", statusCode);

            switch (statusCode) {
                case 204:
                    break;
                case 404:
                    throw new DockerNotFoundException(format("No such "
                            + "container %s", id));
                default:
                    throw new DockerException("Docker Server Error");
            }
        } catch (URISyntaxException | IOException ex) {
            throw new DockerErrorException("Unable to start container", ex);
        }
    }

    @Override
    public void kill(String id) throws DockerNotFoundException,
            DockerErrorException {
        try {
            URI uri = dockerBaseUri(String.format(KILL_ENDPOINT, id))
                    .build();

            HttpResponse response = postJsonRequest(uri);

            int statusCode = response.getStatusLine().getStatusCode();
            LOG.debug("Returned status code {}", statusCode);

            switch (statusCode) {
                case 204:
                    break;
                case 404:
                    throw new DockerNotFoundException(format("No such container"
                            + " %s", id));
                default:
                    throw new DockerException("Docker Server Error");
            }
        } catch (URISyntaxException | IOException ex) {
            throw new DockerErrorException("Unable to start container", ex);
        }
    }

    @Override
    public void stop(String id) throws DockerNotFoundException,
            DockerErrorException {
        try {
            URI uri = dockerBaseUri(String.format(STOP_ENDPOINT, id))
                    .build();

            HttpResponse response = postJsonRequest(uri);

            int statusCode = response.getStatusLine().getStatusCode();
            LOG.debug("Returned status code {}", statusCode);

            switch (statusCode) {
                case 204:
                    break;
                case 404:
                    throw new DockerNotFoundException(format("No such container"
                            + " %s", id));
                default:
                    throw new DockerException("Docker Server Error");
            }
        } catch (URISyntaxException | IOException ex) {
            throw new DockerErrorException("Unable to start container", ex);
        }
    }

    @Override
    public void delete(String id) throws DockerNotFoundException,
            DockerErrorException {
        try {
            URI uri = dockerBaseUri(String.format(DELETE_ENDPOINT, id))
                    .build();
            HttpDelete httpDelete = new HttpDelete(uri);
            httpDelete.addHeader("Content-Type",
                    ContentType.APPLICATION_JSON.toString());

            LOG.debug("Executing request {}", uri);
            HttpResponse response = httpClient.execute(httpDelete);

            int statusCode = response.getStatusLine().getStatusCode();
            LOG.debug("Returned status code {}", statusCode);

            switch (statusCode) {
                case 204:
                    break;
                case 404:
                    throw new DockerNotFoundException(format("No such "
                            + "container %s", id));
                default:
                    throw new DockerErrorException("Docker Server Error");
            }
        } catch (URISyntaxException | IOException ex) {
            throw new DockerErrorException("Unable to delete container", ex);
        }
    }

    @Override
    public InputStream getLogs(String id, LogsFilter filter)
            throws DockerNotFoundException, InvalidRequestException,
            DockerErrorException {
        String tail = filter.getTail() == null
                ? "all"
                : String.valueOf(filter.getTail());
        try {
            URI uri = dockerBaseUri(format(LOGS_ENDPOINT, id))
                    .addParameter("follow", String.valueOf(filter.getFollow()))
                    .addParameter("stdout", String.valueOf(filter.getStdOut()))
                    .addParameter("stderr", String.valueOf(filter.getStdErr()))
                    .addParameter("since", String.valueOf(filter.getSince()))
                    .addParameter("timestamps", String
                            .valueOf(filter.getTimestamps()))
                    .addParameter("tail", tail)
                    .build();

            HttpGet httpGet = new HttpGet(uri);

            LOG.debug("Executing request {}", uri);
            HttpResponse response = httpClient.execute(httpGet);

            int statusCode = response.getStatusLine().getStatusCode();
            LOG.debug("Returned status code {}", statusCode);

            switch (statusCode) {
                case 101:
                case 200:
                    break;
                case 404:
                    throw new DockerNotFoundException(format("No such "
                            + "container %s", id));
                default:
                    throw new DockerException("Docker Server Error");
            }
            return response.getEntity().getContent();

        } catch (URISyntaxException | IOException ex) {
            throw new DockerErrorException(format("Unable to get logs for "
                    + "%s", id), ex);
        }
    }

    private URIBuilder dockerBaseUri(String path) throws URISyntaxException {
        return new URIBuilder(format(BASE_URL_PATTERN, HTTP_PORT, path));
    }

    private HttpResponse postJsonRequest(URI uri) throws IOException {
        return postJsonRequest(uri, null);
    }

    private HttpResponse postJsonRequest(URI uri, Object payload)
            throws IOException {
        HttpPost httpPost = new HttpPost(uri);
        httpPost.addHeader("Content-Type", ContentType.APPLICATION_JSON
                .toString());

        if (payload != null) {
            String jsonPayload = new Gson().toJson(payload);
            LOG.debug("Request payload: {}", jsonPayload);

            HttpEntity entity = new StringEntity(jsonPayload);
            httpPost.setEntity(entity);
        }

        LOG.debug("Executing request {}", uri);
        return httpClient.execute(httpPost);
    }
}
