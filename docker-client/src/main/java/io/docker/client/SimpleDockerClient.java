/*
 * 
 */
package io.docker.client;

import io.docker.exception.DockerErrorException;
import io.docker.exception.DockerNotFoundException;
import io.docker.exception.DockerNotRunningException;
import io.docker.exception.InvalidRequestException;
import io.docker.filter.ListFilter;
import io.docker.filter.LogsFilter;
import io.docker.model.Container;
import io.docker.model.ContainerConfig;
import io.docker.model.ContainerDetail;
import io.docker.model.ContainerInfo;
import io.docker.model.StartConfig;
import java.io.InputStream;
import java.util.List;

/**
 * Simplified docker engine rest client.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public interface SimpleDockerClient {

    /**
     * Pings docker engine, to check if engine is running.
     *
     * @throws DockerNotRunningException
     * @throws DockerErrorException
     */
    void ping() throws DockerNotRunningException, DockerErrorException;

    /**
     *
     * @param filter
     * @return
     * @throws InvalidRequestException
     * @throws DockerErrorException
     */
    List<Container> get(ListFilter filter) throws InvalidRequestException,
            DockerErrorException;

    /**
     *
     * @param id
     * @return
     * @throws DockerNotFoundException
     * @throws DockerErrorException
     */
    ContainerDetail inspect(String id) throws DockerNotFoundException,
            DockerErrorException;

    ContainerInfo create(ContainerConfig config) throws InvalidRequestException,
            DockerErrorException;

    /**
     *
     * @param id
     * @param config
     * @throws DockerNotFoundException
     * @throws InvalidRequestException
     * @throws DockerErrorException
     */
    void start(String id, StartConfig config) throws DockerNotFoundException,
            InvalidRequestException,
            DockerErrorException;

    /**
     *
     * @param id
     * @throws DockerNotFoundException
     * @throws DockerErrorException
     */
    void kill(String id) throws DockerNotFoundException, DockerErrorException;

    /**
     *
     * @param id
     * @throws DockerNotFoundException
     * @throws DockerErrorException
     */
    void stop(String id) throws DockerNotFoundException, DockerErrorException;

    /**
     *
     * @param id
     * @throws DockerNotFoundException
     * @throws DockerErrorException
     */
    void delete(String id) throws DockerNotFoundException, DockerErrorException;

    /**
     *
     * @param id
     * @param filter
     * @return
     * @throws DockerNotFoundException
     * @throws InvalidRequestException
     * @throws DockerErrorException
     */
    InputStream getLogs(String id, LogsFilter filter)
            throws DockerNotFoundException, InvalidRequestException,
            DockerErrorException;
}
