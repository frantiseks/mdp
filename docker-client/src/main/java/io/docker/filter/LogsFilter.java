/*
 * 
 */
package io.docker.filter;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public interface LogsFilter extends RequestFilter {

    boolean getFollow();

    boolean getStdOut();

    boolean getStdErr();

    long getSince();

    boolean getTimestamps();

    Integer getTail();
}
