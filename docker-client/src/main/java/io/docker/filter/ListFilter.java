/*
 *
 */
package io.docker.filter;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public interface ListFilter extends RequestFilter {

    /**
     * Flags is to determine if is needed to fetch all containers, or only
     * running containers.
     *
     * @return
     */
    boolean getAll();

    Integer getLimit();

    String getSinceId();

    String getBeforeId();

    boolean showSize();

    String getStatus();

    Integer getExitedCode();
}
