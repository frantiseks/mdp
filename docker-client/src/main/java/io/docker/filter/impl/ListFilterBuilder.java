/*
 * 
 */
package io.docker.filter.impl;

import io.docker.filter.ListFilter;

public class ListFilterBuilder {

    private boolean all = false;
    private Integer limit = null;
    private String sinceId = null;
    private String beforeId = null;
    private boolean showSize = false;
    private String status = null;
    private Integer exitCode = null;

    public ListFilterBuilder() {
    }

    public ListFilterBuilder setAll(boolean all) {
        this.all = all;
        return this;
    }

    public ListFilterBuilder setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public ListFilterBuilder setSinceId(String sinceId) {
        this.sinceId = sinceId;
        return this;
    }

    public ListFilterBuilder setBeforeId(String beforeId) {
        this.beforeId = beforeId;
        return this;
    }

    public ListFilterBuilder setShowSize(boolean showSize) {
        this.showSize = showSize;
        return this;
    }

    public ListFilterBuilder setStatus(String status) {
        this.status = status;
        return this;
    }

    public ListFilterBuilder setExitCode(Integer exitCode) {
        this.exitCode = exitCode;
        return this;
    }

    public ListFilter build() {
        return new ListFilterImpl(all, limit, sinceId, beforeId,
                showSize, status, exitCode);
    }

}
