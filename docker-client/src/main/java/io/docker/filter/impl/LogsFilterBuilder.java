/*
 * 
 */
package io.docker.filter.impl;

import io.docker.filter.LogsFilter;

public class LogsFilterBuilder {

    private boolean follow = false;
    private boolean stdout = false;
    private boolean stderr = false;
    private boolean timestamps = false;
    private long since = 0;
    private Integer tail = null;

    public LogsFilterBuilder() {
    }

    public LogsFilterBuilder setFollow(boolean follow) {
        this.follow = follow;
        return this;
    }

    public LogsFilterBuilder setStdout(boolean stdout) {
        this.stdout = stdout;
        return this;
    }

    public LogsFilterBuilder setStderr(boolean stderr) {
        this.stderr = stderr;
        return this;
    }

    public LogsFilterBuilder setTimestamps(boolean timestamps) {
        this.timestamps = timestamps;
        return this;
    }

    public LogsFilterBuilder setSince(long since) {
        this.since = since;
        return this;
    }

    public LogsFilterBuilder setTail(Integer tail) {
        this.tail = tail;
        return this;
    }

    public LogsFilter build() {
        return new LogsFilterImpl(follow, stdout, stderr, 
                timestamps, since, tail);
    }

}
