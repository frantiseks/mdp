/*
 * 
 */
package io.docker.filter.impl;

import io.docker.filter.ListFilter;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class ListFilterImpl implements ListFilter {

    private final boolean all;
    private final Integer limit;
    private final String sinceId;
    private final String beforeId;
    private final boolean showSize;
    private final String status;
    private final Integer exitCode;

    public ListFilterImpl(boolean all, Integer limit, String sinceId, 
            String beforeId, boolean showSize, String status,
            Integer exitCode) {
        this.all = all;
        this.limit = limit;
        this.sinceId = sinceId;
        this.beforeId = beforeId;
        this.showSize = showSize;
        this.status = status;
        this.exitCode = exitCode;
    }
    
    

    @Override
    public boolean getAll() {
        return all;
    }

    @Override
    public Integer getLimit() {
        return limit;
    }

    @Override
    public String getSinceId() {
        return sinceId;
    }

    @Override
    public String getBeforeId() {
        return beforeId;
    }

    @Override
    public boolean showSize() {
        return showSize;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public Integer getExitedCode() {
        return exitCode;
    }

}
