/*
 * 
 */
package io.docker.filter.impl;

import io.docker.filter.LogsFilter;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class LogsFilterImpl implements LogsFilter {

    private final boolean follow;
    private final boolean stdout;
    private final boolean stderr;
    private final boolean timestamps;
    private final long since;
    private final Integer tail;

    public LogsFilterImpl(boolean follow, boolean stdout, boolean stderr,
            boolean timestamps, long since, Integer tail) {
        this.follow = follow;
        this.stdout = stdout;
        this.stderr = stderr;
        this.timestamps = timestamps;
        this.since = since;
        this.tail = tail;
    }

    @Override
    public boolean getFollow() {
        return follow;
    }

    @Override
    public boolean getStdOut() {
        return stdout;
    }

    @Override
    public boolean getStdErr() {
        return stderr;
    }

    @Override
    public long getSince() {
        return since;
    }

    @Override
    public boolean getTimestamps() {
        return timestamps;
    }

    @Override
    public Integer getTail() {
        return tail;
    }

}
