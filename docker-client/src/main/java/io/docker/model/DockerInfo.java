/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

/**
 * @author Frantisek Spacek
 */
public class DockerInfo {

    @SerializedName("Debug")
    private boolean debug;

    @SerializedName("Containers")
    private int containers;

    @SerializedName("Images")
    private int images;

    @SerializedName("Driver")
    private String driver;

    @SerializedName("NCPU")
    private int ncpu;

    @SerializedName("NFd")
    private int nfd;

    @SerializedName("NGRoutines")
    private int NGRoutines;

    @SerializedName("MemoryLimit")
    private boolean memoryLimit;

    @SerializedName("SwapLimit")
    private boolean swapLimit;

    @SerializedName("MemTotal")
    private long memTotal;

    @SerializedName("Name")
    private String name;

    @SerializedName("IPv4Forwarding")
    private boolean IPv4Forwarding;

    @SerializedName("ExecutionDriver")
    private String executionDriver;

    @SerializedName("NEventsListener")
    private int NEventsListener;

    @SerializedName("KernelVersion")
    private String kernelVersion;

    @SerializedName("IndexServerAddress")
    private String indexServerAddress;

    @SerializedName("ID")
    private String id;

    @SerializedName("Labels")
    private String[] labels;

    @SerializedName("OperatingSystem")
    private String operatingSystem;

    public boolean isDebug() {
        return debug;
    }

    public int getContainers() {
        return containers;
    }

    public int getImages() {
        return images;
    }

    public String getDriver() {
        return driver;
    }

    public int getNcpu() {
        return ncpu;
    }

    public int getNfd() {
        return nfd;
    }

    public int getNGRoutines() {
        return NGRoutines;
    }

    public boolean isMemoryLimit() {
        return memoryLimit;
    }

    public boolean isSwapLimit() {
        return swapLimit;
    }

    public long getMemTotal() {
        return memTotal;
    }

    public String getName() {
        return name;
    }

    public boolean getIPv4Forwarding() {
        return IPv4Forwarding;
    }

    public String getExecutionDriver() {
        return executionDriver;
    }

    public int getNEventsListener() {
        return NEventsListener;
    }

    public String getKernelVersion() {
        return kernelVersion;
    }

    public String getIndexServerAddress() {
        return indexServerAddress;
    }

    public String getId() {
        return id;
    }

    public String[] getLabels() {
        return labels;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    @Override
    public String toString() {
        return "DockerInfo{" + "debug=" + debug + ", containers=" + containers
                + ", images=" + images + ", driver=" + driver + ", ncpu="
                + ncpu + ", nfd=" + nfd + ", NGRoutines=" + NGRoutines
                + ", memoryLimit=" + memoryLimit + ", swapLimit=" + swapLimit
                + ", memTotal=" + memTotal + ", name=" + name
                + ", IPv4Forwarding=" + IPv4Forwarding + ", executionDriver="
                + executionDriver + ", NEventsListener=" + NEventsListener
                + ", kernelVersion=" + kernelVersion + ", indexServerAddress="
                + indexServerAddress + ", id=" + id
                + ", labels=" + Arrays.toString(labels)
                + ", operatingSystem=" + operatingSystem + '}';
    }

}
