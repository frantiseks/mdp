/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class StartConfig {

    private static final String BIND_FORMAT = "%s:%s:%s";
    @SerializedName("Binds")
    private List<String> binds;

    public List<String> getBinds() {
        return binds;
    }

    public void setBinds(List<String> binds) {
        this.binds = binds;
    }

    public void addBind(String host, String container, boolean allowRW) {
        if (Objects.isNull(binds)) {
            binds = new ArrayList<>();
        }

        binds.add(String
                .format(BIND_FORMAT, host, container, allowRW ? "rw" : "ro"));

    }

}
