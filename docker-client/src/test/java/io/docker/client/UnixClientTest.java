package io.docker.client;

import io.docker.enums.Protocol;

/**
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class UnixClientTest extends TCPClientTest {

    @Override
    public void setUp() {
        super.client = new DockerDefaultClient.Builder()
                .withProtocol(Protocol.UNIX)
                .withHost("/var/run/docker.sock")
                .withPort(80)
                .build();
    }

}
