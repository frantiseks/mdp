/*
 * 
 */
package io.docker.client;

import io.docker.exception.DockerErrorException;
import io.docker.exception.DockerNotFoundException;
import io.docker.filter.impl.ListFilterBuilder;
import io.docker.filter.impl.LogsFilterBuilder;
import io.docker.model.Container;
import io.docker.model.ContainerConfig;
import io.docker.model.ContainerDetail;
import io.docker.model.ContainerInfo;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class SimpleDockerClientImplTest {

    private final List<String> containerIds = new ArrayList<>();

    private SimpleDockerClient client;

    @Before
    public void setUp() {
        System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY,
                "DEBUG");
        client = new SimpleDockerClientImpl("/var/run/docker.sock");
    }

    @After
    public void tearDown() {
        containerIds.stream().forEach((id) -> {
            try {
                ContainerDetail container = client.inspect(id);

                if (container.getState().isRunning()) {
                    client.kill(id);
                }

                client.delete(id);
            } catch (DockerErrorException ex) {
                System.out.println(ex);
            }
        });
    }

    @Test
    public void testPing() throws Exception {
        client.ping();
    }

    @Test
    public void testGet() {
        List<Container> containers = client
                .get(new ListFilterBuilder().build());
        assertTrue(containers.isEmpty());

        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        assertNotNull(info);
        assertNotNull(info.getId());

        containerIds.add(info.getId());
        client.start(info.getId(), null);
        ContainerDetail detail = client.inspect(info.getId());
        assertTrue(detail.getState().isRunning());

        containers = client.get(new ListFilterBuilder().build());
        assertFalse(containers.isEmpty());
    }

    @Test
    public void testInspect() {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        assertNotNull(info);
        assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);

        ContainerDetail detail = client.inspect(id);
        assertFalse(detail.getId().isEmpty());
        assertFalse(detail.getCreated().isEmpty());
    }

    @Test
    public void testCreate() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        containerIds.add(info.getId());
    }

    @Test
    public void testStart() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);

        ContainerDetail detail = client.inspect(id);
        Assert.assertTrue(detail.getState().isRunning());
    }

    @Test
    public void testStop() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);
        ContainerDetail detail = client.inspect(id);
        Assert.assertFalse(detail.getState().getStartedAt().isEmpty());

        client.stop(id);
        detail = client.inspect(id);
        Assert.assertFalse(detail.getState().isRunning());
    }

    @Test(expected = DockerNotFoundException.class)
    public void testDelete() throws Exception {
        ContainerConfig config = new ContainerConfig.Builder()
                .setImage("busybox")
                .build();

        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        client.delete(id);

        //throws DockerNotFoundException
        client.inspect(id);
    }

    @Test
    public void testKill() {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);

        ContainerDetail detail = client.inspect(id);
        Assert.assertTrue(detail.getState().isRunning());

        client.kill(id);
    }

    @Test
    public void testGetLogs() {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        config.setCmd(new String[]{"date"});
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);
        InputStream logStream = client.getLogs(id,
                new LogsFilterBuilder()
                .setStdout(true)
                .setStderr(true)
                .build());

        StringBuilder outputBuilder = new StringBuilder();

        Scanner scanner = new Scanner(logStream);
        while (scanner.hasNextLine()) {
            outputBuilder.append(scanner.nextLine());
        }

        assertFalse(outputBuilder.toString().isEmpty());
    }

}
