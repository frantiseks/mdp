package io.docker.client;

import io.docker.exception.DockerException;
import io.docker.exception.DockerNotFoundException;
import io.docker.exception.DockerNotRunningException;
import io.docker.enums.Protocol;
import io.docker.model.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author frantisek
 */
public class TCPClientTest {

    protected final Logger LOG = LoggerFactory.getLogger(TCPClientTest.class);
    protected final List<String> containerIds = new ArrayList<>();
    protected DockerClient client;

    @Before
    public void setUp() {
        System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "DEBUG");
        client = new DockerDefaultClient.Builder()
                .withProtocol(Protocol.TCP)
                .withHost("localhost")
                .withPort(4243)
                .build();
    }

    @After
    public void tearDown() {
        containerIds.stream().forEach((id) -> {
            try {
                ContainerDetail container = client.inspect(id);

                if (container.getState().isRunning()) {
                    client.kill(id);
                }

                client.delete(id);
            } catch (DockerException | DockerNotRunningException ex) {
                LOG.error("Tear Down exception", ex);
            }

            LOG.info("Container {} deleted", id);
        });
    }

    @Test
    public void testGet() throws Exception {
        List<Container> containers = client.get(true);
        Assert.assertTrue(containers.isEmpty());

        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        containerIds.add(info.getId());
        client.start(info.getId(), null);
        ContainerDetail detail = client.inspect(info.getId());
        Assert.assertTrue(detail.getState().isRunning());

        containers = client.get(false);
        Assert.assertFalse(containers.isEmpty());
    }

    @Test
    public void testInspect() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);

        ContainerDetail detail = client.inspect(id);
        Assert.assertFalse(detail.getId().isEmpty());
        Assert.assertFalse(detail.getCreated().isEmpty());

    }

    @Test
    public void testCreate() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        containerIds.add(info.getId());
    }

    @Test
    public void testStart() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);

        ContainerDetail detail = client.inspect(id);
        Assert.assertTrue(detail.getState().isRunning());
    }

    @Test
    public void testStop() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);
        ContainerDetail detail = client.inspect(id);
        Assert.assertFalse(detail.getState().getStartedAt().isEmpty());

        client.stop(id);
        detail = client.inspect(id);
        Assert.assertFalse(detail.getState().isRunning());
    }

    @Test(expected = DockerNotFoundException.class)
    public void testDelete() throws Exception {
        ContainerConfig config = new ContainerConfig.Builder()
                .setImage("busybox")
                .build();

        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        client.delete(id);

        //throws DockerNotFoundException
        client.inspect(id);
    }

    @Test
    public void testRestart() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);

        ContainerDetail detail = client.inspect(id);
        Assert.assertTrue(detail.getState().isRunning());

        client.restart(id);
    }

    @Test
    public void testKill() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);

        ContainerDetail detail = client.inspect(id);
        Assert.assertTrue(detail.getState().isRunning());

        client.kill(id);
    }

    @Test
    public void testGetVersion() throws Exception {
        DockerVersion version = client.getVersion();
        Assert.assertFalse(version.getGitCommit().isEmpty());
        Assert.assertFalse(version.getGoVersion().isEmpty());
        Assert.assertFalse(version.getVersion().isEmpty());
        Assert.assertFalse(version.getKernelVersion().isEmpty());
        Assert.assertFalse(version.getArch().isEmpty());
        Assert.assertFalse(version.getOs().isEmpty());
    }

    @Test
    public void testGetInfo() throws Exception {
        DockerInfo info = client.getInfo();
        Assert.assertFalse(info.getDriver().isEmpty());
        Assert.assertFalse(info.getExecutionDriver().isEmpty());
        Assert.assertFalse(info.getName().isEmpty());
        Assert.assertFalse(info.getKernelVersion().isEmpty());
        Assert.assertFalse(info.getId().isEmpty());

    }

    @Test
    public void testGetProcesses() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);
        ContainerProcesses processes = client.getProcesses(id, "aux");
        Assert.assertFalse(processes.getProcesses().isEmpty());
    }

    @Test
    public void testGetLogs() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        config.setCmd(new String[]{"date"});
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);
        String output = client.getLogs(id, true, true);
        Assert.assertFalse(output.isEmpty());
    }

    @Test
    public void testAttach() throws Exception {
        ContainerConfig config = new ContainerConfig();
        config.setImage("busybox");
        config.setCmd(new String[]{"date"});
        ContainerInfo info = client.create(config);
        Assert.assertNotNull(info);
        Assert.assertNotNull(info.getId());

        String id = info.getId();
        containerIds.add(id);

        client.start(id, null);

        InputStream stream = client.attach(id, true, true);

        try (java.util.Scanner s = new java.util.Scanner(stream)) {
            System.out.println(s.useDelimiter("\\A").hasNext() ? s.next() : "");
        }

    }

}
