/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.commons.util;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author František Špaček
 */
public class FileUtil {

  /**
   * flatten submission hierarchy
   *
   * @param parentDir - parent dir
   * @param dirs      - dirs to flatten
   * @param filter    - extension filter
   * @throws IOException
   */
  public static void getFilesFromDirs(File parentDir, File[] dirs,
      GenericExtFilter filter) throws IOException {
    for (File dir : dirs) {
      File[] files = dir.listFiles(filter);

      for (File file : files) {

        if (!file.isDirectory()) {
          FileUtils.copyFileToDirectory(file, parentDir);
        }
      }

      FileUtils.deleteDirectory(dir);
    }
  }

  /**
   * build source list command
   *
   * @param sourceFiles   - set of sources
   * @param workspacePath - absolute workspace path
   * @return
   */
  public static String buildSourcesList(Set<String> sourceFiles,
      String workspacePath) {
    StringBuilder sourcesList = new StringBuilder();
    sourceFiles.stream().map((source) -> {
      sourcesList.append(workspacePath);
      sourcesList.append(File.separator);
      sourcesList.append(source);
      return source;
    }).forEach(s -> {
      sourcesList.append(" ");
    });

    return sourcesList.toString();
  }
}
