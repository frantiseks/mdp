/*
 *
 */

package cz.utb.fai.mdp.commons.dto;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public interface SubmissionData {

  String getUuid();
}
