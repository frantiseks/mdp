/*
 * 
 */
package cz.utb.fai.mdp.commons.dto.impl;

import cz.utb.fai.mdp.commons.dto.ExecutionResult;

/**
 * Immutable implementation of {@link ExecutionResult}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class ExecutionResultImpl implements ExecutionResult {

    private final String output;
    private final long duration;
    private final boolean terminated;

    public ExecutionResultImpl(String output, long duration,
            boolean terminated) {
        this.output = output;
        this.duration = duration;
        this.terminated = terminated;
    }

    @Override
    public String getOutput() {
        return output;
    }

    @Override
    public long getDuration() {
        return duration;
    }

    @Override
    public boolean isTerminated() {
        return terminated;
    }

    @Override
    public String toString() {
        return "ExecutionResultImpl{" + "output=" + output
                + ", duration=" + duration + ", terminated=" + terminated + '}';
    }

}
