/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.mdp.commons.dto;

/**
 * Contains output and state of compilation.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public interface CompilationResult {

    /**
     * Gets output of compilation.
     *
     * @return compilation output, if no output {@code null}
     */
    String getOutput();

    /**
     * Gets flag if compilation failed.
     *
     * @return {@code false} if there was no error, otherwise {@code true}
     */
    boolean isFailed();

}
