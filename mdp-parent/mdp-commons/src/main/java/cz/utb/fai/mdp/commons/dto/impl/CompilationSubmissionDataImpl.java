/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.commons.dto.impl;

import cz.utb.fai.mdp.commons.dto.CompilationResult;
import cz.utb.fai.mdp.commons.dto.CompilationSubmissionData;

import java.util.Objects;

/**
 * Immutable implementation of {@link CompilationSubmissionData}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class CompilationSubmissionDataImpl extends SubmissionDataImpl
    implements CompilationSubmissionData {

  private final CompilationResult compilationResult;

  public CompilationSubmissionDataImpl(final String uuid,
      final CompilationResult compilationResult) {
    super(uuid);
    this.compilationResult = compilationResult;
  }

  @Override
  public final CompilationResult getResult() {
    return compilationResult;
  }

  @Override
  public final String toString() {
    return "CompilationSubmissionDataImpl{" + "compilationResult="
        + Objects.toString(compilationResult) + '}';
  }

}
