/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.mdp.commons.util;

import cz.utb.fai.mdp.commons.enums.Extension;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Collection;
import java.util.Optional;
import static org.apache.commons.io.FileUtils.listFiles;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

/**
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class WorkspaceUtil {

    public static String workspacePath(String basePath, String workspaceUUID) {
        String workspacePath = basePath;

        if (!workspacePath.endsWith("/")) {
            workspacePath += File.separator;
        }
        workspacePath += workspaceUUID;

        return workspacePath;
    }

    public static File workspace(String basePath, String workspaceUUID) {
        String workspacePath = basePath;

        if (!workspacePath.endsWith("/")) {
            workspacePath += File.separator;
        }
        workspacePath += workspaceUUID;

        return new File(workspacePath);
    }

    public static void deleteWorkspace(String basePath, String workspaceUUID) {
        FileUtils.deleteQuietly(workspace(basePath, workspaceUUID));
    }

    public static Optional<File> findBinary(File workspace) {
        final SuffixFileFilter filter = new SuffixFileFilter(
                Extension.BUILD.toString(),
                IOCase.INSENSITIVE);

        Collection<File> files = listFiles(workspace, filter, TrueFileFilter.INSTANCE);

        return files.stream().findFirst();
    }
}
