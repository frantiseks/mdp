/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.commons.enums;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public enum Language {

  C("C"),
  CPP("CPP"),
  JAVA("JAVA"),
  CSHARP("CSHARP"),
  PYTHON("PYTHON"),
  CUDA("CUDA");

  private final String code;
  private static final Map<String, Language> map = new HashMap<>();

  static {
    for (Language lang : Language.values()) {
      map.put(lang.code, lang);
    }
  }

  private Language(String code) {
    this.code = code;
  }

  public static Language fromCode(String code) {
    if (code == null || code.isEmpty()) {
      throw new IllegalArgumentException("Code cannot be non-empty"
          + " string");
    }

    return map.get(code.toUpperCase());
  }

  public static String getCode(Language language) {
    return language.code;
  }

  @Override
  public String toString() {
    return super.toString().toLowerCase();
  }

}
