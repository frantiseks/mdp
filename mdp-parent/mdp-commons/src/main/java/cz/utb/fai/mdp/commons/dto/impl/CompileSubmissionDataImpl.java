/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.commons.dto.impl;

import cz.utb.fai.mdp.commons.dto.CompileSubmissionData;
import cz.utb.fai.mdp.commons.enums.Language;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class CompileSubmissionDataImpl extends SubmissionDataImpl
    implements CompileSubmissionData {

  private final Language language;
  private final URL download;
  private final URL upload;

  public CompileSubmissionDataImpl(String uuid, Language language,
      String download, String upload) throws MalformedURLException {
    super(uuid);
    this.download = new URL(download);
    this.upload = new URL(upload);
    this.language = language;
  }

  @Override
  public Language getLanguage() {
    return language;
  }

  @Override
  public final URL getDownloadUrl() {
    return download;
  }

  @Override
  public final URL getUploadUrl() {
    return upload;
  }

  @Override
  public final String toString() {
    return "CompileSubmissionDataImpl{" + "download="
        + Objects.toString(download) + ", upload="
        + Objects.toString(upload) + '}';
  }

}
