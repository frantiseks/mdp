/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.mdp.commons.dto;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public interface ExecutionResult {

    /**
     * Gets output of execution from sandbox environment.
     *
     * @return execution output, if no output {@code null}
     */
    String getOutput();

    /**
     * Gets execution time.
     *
     * @return time in milliseconds, bigger than 0
     */
    long getDuration();

    /**
     * Gets flag if execution was terminated.
     *
     * @return {@code true} if execution was terminated, otherwise {@code false}
     */
    boolean isTerminated();

}
