/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.mdp.commons.spi;

import cz.utb.fai.mdp.commons.dto.CompilationResult;
import cz.utb.fai.mdp.commons.dto.ExecutionResult;
import java.io.File;

/**
 * Interface for language processors plug-ins.
 *
 * @author František Špaček
 */
public interface Processor {

    /**
     * Method for check if is compilation process required
     *
     * @return
     */
    boolean isCompilationRequired();

    /**
     * Build compile command.
     *
     * @param submissionWorkspace - submissionWorkspace
     * @return
     */
    String buildCompileCommand(File submissionWorkspace);

    /**
     *
     * @param compileResult
     * @return
     */
    boolean isCompilationSuccess(CompilationResult compileResult);

    /**
     * Execution command
     *
     * @param submissionWorkspace
     * @return
     */
    String buildExecuteCommand(File submissionWorkspace);

    /**
     *
     * @param executionResult
     * @return
     */
    boolean isExecutionSuccess(ExecutionResult executionResult);
}
