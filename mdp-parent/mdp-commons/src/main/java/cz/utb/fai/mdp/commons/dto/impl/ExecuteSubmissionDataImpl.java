/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.commons.dto.impl;

import cz.utb.fai.mdp.commons.dto.ExecuteSubmissionData;
import cz.utb.fai.mdp.commons.enums.Language;

import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class ExecuteSubmissionDataImpl extends SubmissionDataImpl
    implements ExecuteSubmissionData {

  private final Language language;
  private final String arguments;
  private final URL download;

  public ExecuteSubmissionDataImpl(String uuid, Language language,
      String arguments, String downloadUrl) throws MalformedURLException {
    super(uuid);
    this.arguments = arguments;
    this.download = new URL(downloadUrl);
    this.language = language;
  }

  @Override
  public Language getLanguage() {
    return language;
  }

  @Override
  public final String getArguments() {
    return arguments;
  }

  @Override
  public final URL downloadUrl() {
    return download;
  }

  @Override
  public final String toString() {
    return "ExecuteSubmissionDataImpl{" + "arguments=" + arguments + '}';
  }
}
