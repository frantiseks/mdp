/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.commons.dto.impl;

import cz.utb.fai.mdp.commons.dto.ExecutionResult;
import cz.utb.fai.mdp.commons.dto.ExecutionSubmissionData;

import java.util.Objects;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class ExecutionSubmissionDataImpl extends SubmissionDataImpl
    implements ExecutionSubmissionData {

  private final ExecutionResult result;

  public ExecutionSubmissionDataImpl(String uuid, ExecutionResult result) {
    super(uuid);
    this.result = result;
  }

  @Override
  public ExecutionResult getResult() {
    return result;
  }

  @Override
  public String toString() {
    return "ExecutionSubmissionDataImpl{" + "result="
        + Objects.toString(result) + '}';
  }

}
