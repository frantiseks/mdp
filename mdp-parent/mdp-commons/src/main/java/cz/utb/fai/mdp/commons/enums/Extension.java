/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.commons.enums;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author František Špaček
 */
public enum Extension {

  /**
   * binary file
   */
  BUILD("build"),
  /**
   * output file
   */
  OUT("out"),
  /**
   * input file
   */
  IN("in");

  private final String code;
  private static final Map<String, Extension> map = new HashMap<>();

  static {
    for (Extension extension : Extension.values()) {
      map.put(extension.code, extension);
    }
  }

  private Extension(String code) {
    this.code = code;
  }

  public static Extension fromCode(String code) {
    if (code == null || code.isEmpty()) {
      throw new IllegalArgumentException("Code cannot be non-empty "
          + "string");
    }

    return map.get(code);
  }

  @Override
  public String toString() {
    return super.toString().toLowerCase();
  }
}
