
package cz.utb.fai.mdp.processor;

import cz.utb.fai.mdp.commons.dto.CompilationResult;
import cz.utb.fai.mdp.commons.dto.ExecutionResult;
import cz.utb.fai.mdp.commons.enums.Extension;
import cz.utb.fai.mdp.commons.enums.Language;
import cz.utb.fai.mdp.commons.spi.Processor;
import cz.utb.fai.mdp.commons.spi.ProcessorInfo;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collection;
import java.util.Objects;

/**
 * Processor implementation for CUDA compilation and execution.
 *
 * @author František Špaček
 */
@ProcessorInfo(language = Language.CUDA, version = 1.0)
public final class CudaProcessor implements Processor {

  private static final Logger LOG = LoggerFactory
      .getLogger(CudaProcessor.class);

  private static final String MAKE_COMMAND = "make -C %s";
  private static final String EXE_COMMAND = "bash -c './%s";
  private static final String MAKE_FILE = "makefile";

  @Override
  public boolean isCompilationRequired() {
    return true;
  }

  @Override
  public String buildCompileCommand(final File submissionWorkspace) {
    Collection<File> files = FileUtils.listFiles(submissionWorkspace,
        FileFilterUtils.nameFileFilter(MAKE_FILE, IOCase.INSENSITIVE),
        TrueFileFilter.INSTANCE);

    String command = "";
    if (files.size() > 0) {
      File makeFile = files.iterator().next();
      command = String.format(MAKE_COMMAND,
          submissionWorkspace.toPath().relativize(makeFile.getParentFile()
              .toPath()));
    }

    LOG.debug("Builded command for compilation {}", command);
    
    return command;
  }

  @Override
  public boolean isCompilationSuccess(final CompilationResult compileResult) {
    Objects.requireNonNull(compileResult, "Compile result cannot be null");

    boolean success = true;

    if (StringUtils.isNotBlank(compileResult.getOutput())
        && compileResult.getOutput().contains("error")) {
      success &= false;
    }
    return success;
  }

  @Override
  public String buildExecuteCommand(final File submissionWorkspace) {
    Collection<File> files = FileUtils
        .listFiles(submissionWorkspace, FileFilterUtils
            .suffixFileFilter("." + Extension.BUILD.toString()),
            TrueFileFilter.INSTANCE);

    String command = "BUILD_NOT_FOUND";
    if (files.size() > 0) {
      File buildFile = files.iterator().next();
      command = String.format(EXE_COMMAND, submissionWorkspace.toPath()
          .relativize(buildFile.toPath())
      );
    }
    return command;
  }

  @Override
  public boolean isExecutionSuccess(final ExecutionResult executionResult) {
    Objects.requireNonNull(executionResult, "Compile result cannot be null");
    boolean success = true;

    if (StringUtils.isNotBlank(executionResult.getOutput())
        && executionResult.getOutput().contains("killed")) {
      success &= false;
    }
    return success;
  }

}
