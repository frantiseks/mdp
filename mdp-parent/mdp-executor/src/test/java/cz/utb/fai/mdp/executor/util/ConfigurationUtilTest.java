/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.util;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

/**
 * Tests for {@link ConfigurationUtil}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class ConfigurationUtilTest {

  @Before
  public void setUp() {
    ConfigurationUtil.init();
  }

  @Test
  public void testInit() {
  }

  @Test
  public void testInitDebug() {
  }

  @Test
  public void testGetProperties() {
    Properties loadedProperties = ConfigurationUtil.getProperties();
    assertNotNull(loadedProperties);
  }

  @Test
  public void testGetProperty() {
  }

}
