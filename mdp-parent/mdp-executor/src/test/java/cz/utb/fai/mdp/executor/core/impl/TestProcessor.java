/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.core.impl;

import cz.utb.fai.mdp.commons.dto.CompilationResult;
import cz.utb.fai.mdp.commons.dto.ExecutionResult;
import cz.utb.fai.mdp.commons.enums.Extension;
import cz.utb.fai.mdp.commons.spi.Processor;
import cz.utb.fai.mdp.commons.util.DirFilter;
import cz.utb.fai.mdp.commons.util.FileUtil;
import cz.utb.fai.mdp.commons.util.GenericExtFilter;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class TestProcessor implements Processor {

  private static final Logger LOG = LoggerFactory.getLogger(Processor.class);
  private static final String COMPILER_C_COMMAND = "gcc -Wall -std=c99";
  private static final String COMPILER_C_OPTIONAL_ARGS = "-fopenmp";
  private static final String VALGRIND_COMMAND = "valgrind --leak-check=full ./%s.%s";

  private static final GenericExtFilter SOURCE_FILES_FILTER
      = new GenericExtFilter(new String[]{"c", "h"});

  private static final double W1 = 0.5;
  private static final double W2 = 0.5;

  @Override
  public boolean isCompilationRequired() {
    return true;
  }

  @Override
  public String buildCompileCommand(File submissionWorkspace) {
    String command = COMPILER_C_COMMAND;
    File[] sourcesDirs = submissionWorkspace.listFiles(new DirFilter());

    if (sourcesDirs.length > 0) {
      try {
        FileUtil.getFilesFromDirs(submissionWorkspace,
            sourcesDirs, SOURCE_FILES_FILTER);
      } catch (IOException ex) {
        LOG.error("Copy files from dir exception", ex);
      }
    }
    String outputFile = String.format("%s.%s",
        submissionWorkspace.getName(), Extension.BUILD.toString());

    command += String.format(" %s -o %s %s",
        getFileList(submissionWorkspace), outputFile,
        COMPILER_C_OPTIONAL_ARGS);

    return command;
  }

  @Override
  public boolean isCompilationSuccess(CompilationResult compilationResult) {
    Objects.requireNonNull(compilationResult, "Compile result cannot be null");

    boolean success = true;

    if (StringUtils.isNotBlank(compilationResult.getOutput())
        && compilationResult.getOutput().contains("error")) {
      success &= false;
    }
    return success;
  }

  @Override
  public String buildExecuteCommand(File submissionWorkspace) {
    return String.format(VALGRIND_COMMAND, submissionWorkspace.getName(),
        Extension.BUILD.toString());
  }

  @Override
  public boolean isExecutionSuccess(ExecutionResult executionResult) {
    Objects.requireNonNull(executionResult, "Execution result cannot be null");
    boolean success = true;

    if (executionResult.isTerminated()) {
      return false;
    }

    if (StringUtils.isBlank(executionResult.getOutput())) {
      return false;
    }

    if (executionResult.getOutput().contains("killed")) {
      return false;
    }
    return success;
  }

  private String getFileList(File workspace) {
    Set<String> sourceFiles = new HashSet<>(Arrays
        .asList(workspace.list(SOURCE_FILES_FILTER)));

    StringBuilder sb = new StringBuilder();
    sourceFiles.stream().map((s) -> {
      sb.append(s);
      return s;
    }).forEach((_item) -> {
      sb.append(" ");
    });

    return sb.toString();
  }

  public String getOutputFromValgrindOutput(String valgrindOutput) {
    StringBuilder programOutput = new StringBuilder();

    Scanner scanner = new Scanner(valgrindOutput);
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      if (!line.startsWith("==")) {
        programOutput.append(line
            .replaceFirst("[=]{2}[0-9]{0,4}[=]{2}", "").trim());
        programOutput.append(System.getProperty("line.separator"));
      }
    }
    return programOutput.toString();
  }

}
