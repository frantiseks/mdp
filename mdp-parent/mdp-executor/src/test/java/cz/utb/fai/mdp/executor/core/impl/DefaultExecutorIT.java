/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.core.impl;

import cz.utb.fai.mdp.commons.dto.CompilationResult;
import cz.utb.fai.mdp.commons.dto.ExecutionResult;
import cz.utb.fai.mdp.executor.core.Executor;

import io.docker.model.Container;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.docker.client.SimpleDockerClient;
import io.docker.client.SimpleDockerClientImpl;
import io.docker.filter.impl.ListFilterBuilder;

import java.util.concurrent.Executors;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class DefaultExecutorIT {

  private static final String TEMP_PATH
      = System.getProperty("java.io.tmpdir");
  private static final String DOCKER_HOST = "/var/run/docker.sock";
  private static final String DOCKER_IMAGE = "frantiseks/apac";

  private static final String HELLO_WORLD_IN_C = "#include<stdio.h>\n"
      + "\n"
      + "main()\n"
      + "{\n"
      + "    printf(\"Hello World\");\n"
      + "\n"
      + "}";

  private Executor instance;
  private final SimpleDockerClient client
      = new SimpleDockerClientImpl(DOCKER_HOST);

  private boolean cleaned = false;

  @Before
  public void setUp() {
    File workspace = new File(TEMP_PATH + File.separator
        + UUID.randomUUID());
    workspace.mkdir();

    try (FileWriter writer = new FileWriter(workspace.getAbsolutePath()
        + File.separator + "test.c")) {
      writer.append(HELLO_WORLD_IN_C);
    } catch (IOException ex) {
      System.out.println(ex);
    }

    instance = new DefaultExecutor(client, new TestProcessor(),
        Executors.newSingleThreadExecutor(), workspace);
  }

  @After
  public void tearDown() {
    if (!cleaned) {
      instance.cleanUp();
    }
  }

  @Test
  public void testCompile() {
    CompilationResult result = instance.compile();
    Assert.assertFalse(result.isFailed());
  }

  @Test
  public void testExecute() {
    ExecutionResult result = instance.execute("-a");
    Assert.assertFalse(result.isTerminated());
  }

  @Test
  public void testCleanUp() {
    instance.compile();
    instance.execute("-a");
    instance.cleanUp();
    List<Container> containers = client
        .get(new ListFilterBuilder().setAll(true).build());

    Assert.assertTrue(containers.stream()
        .filter(c -> c.getImage().contains(DOCKER_IMAGE))
        .collect(Collectors.toList()).isEmpty());

    cleaned = true;

  }

}
