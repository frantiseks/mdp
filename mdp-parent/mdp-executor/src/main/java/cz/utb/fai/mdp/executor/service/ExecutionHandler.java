/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.service;

import cz.utb.fai.mdp.commons.dto.ExecuteSubmissionData;
import cz.utb.fai.mdp.commons.dto.ExecutionResult;
import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.commons.dto.impl.ExecutionSubmissionDataImpl;
import cz.utb.fai.mdp.executor.core.Executor;
import cz.utb.fai.mdp.executor.core.ExecutorFactory;
import cz.utb.fai.mdp.executor.kafka.DataProducer;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class ExecutionHandler implements SubmissionHandler {

  private final ExecutorFactory executorFactory;
  private final DataProducer producer;

  public ExecutionHandler(final ExecutorFactory executorFactory,
      final DataProducer dataProducer) {
    this.executorFactory = executorFactory;
    this.producer = dataProducer;
  }

  @Override
  public void handle(final SubmissionData data) {
    if (!(data instanceof ExecuteSubmissionData)) {
      return;
    }
    ExecuteSubmissionData executeData = (ExecuteSubmissionData) data;

    Executor executor = executorFactory.makeExecutor(executeData.getUuid(),
        executeData.getLanguage());

    ExecutionResult result = executor.execute(executeData.getArguments());

    SubmissionData executionData
        = new ExecutionSubmissionDataImpl(data.getUuid(), result);
    producer.sendData(executionData);
  }
}
