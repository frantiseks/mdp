/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.kafka.impl;

import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.executor.kafka.DataProducer;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of {@link DataProducer}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class DefaultDataProducer implements DataProducer {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(DefaultDataProducer.class);

  /**
   * Destination topic for messages.
   */
  private final String topic;

  /**
   * Kafka producer instance.
   */
  private final Producer producer;

  /**
   * Creates instance of default data producer.
   *
   * @param destinationTopic producer destination topic
   * @param kafkaProducer    instance of Kafka producer.
   */
  public DefaultDataProducer(final String destinationTopic,
      final Producer<String, SubmissionData> kafkaProducer) {
    this.topic = destinationTopic;
    this.producer = kafkaProducer;
  }

  @Override
  public void sendData(final SubmissionData data) {
    LOG.debug("Sending message {} to {}", data, topic);
    producer.send(new ProducerRecord(topic, data.getUuid(), data));
  }

  @Override
  public void shutdown() {
    if (producer != null) {
      producer.close();
    }
  }
}
