/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor;

import static cz.utb.fai.mdp.executor.util.ConfigurationUtil.getProperty;

import cz.utb.fai.mdp.commons.dto.CompileSubmissionData;
import cz.utb.fai.mdp.commons.dto.ExecuteSubmissionData;
import cz.utb.fai.mdp.executor.core.ExecutorFactory;
import cz.utb.fai.mdp.executor.core.ProcessorFactory;
import cz.utb.fai.mdp.executor.core.impl.ExecutorFactoryImpl;
import cz.utb.fai.mdp.executor.core.impl.ProcessorFactoryImpl;
import cz.utb.fai.mdp.executor.kafka.DataProducer;
import cz.utb.fai.mdp.executor.kafka.impl.DefaultConsumer;
import cz.utb.fai.mdp.executor.kafka.SubmissionConsumer;
import cz.utb.fai.mdp.executor.kafka.SubmissionWorkerFactory;
import cz.utb.fai.mdp.executor.kafka.impl.DefaultDataProducer;
import cz.utb.fai.mdp.executor.service.CompilationHandler;
import cz.utb.fai.mdp.executor.kafka.impl.DefaultWorkerFactory;
import cz.utb.fai.mdp.executor.kafka.impl.JsonMessageSerializer;
import cz.utb.fai.mdp.executor.service.ExecutionHandler;
import cz.utb.fai.mdp.executor.service.SubmissionHandler;
import cz.utb.fai.mdp.executor.util.ConfigurationUtil;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.docker.client.SimpleDockerClient;
import io.docker.client.SimpleDockerClientImpl;
import io.docker.exception.DockerErrorException;
import io.docker.exception.DockerNotRunningException;
import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.javaapi.consumer.ConsumerConnector;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Bootstrap class for Executor application.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class ExecutorRunner {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(ExecutorRunner.class);

  /**
   * Private constructor of runner class.
   */
  private ExecutorRunner() {

  }

  /**
   * Main method of Executor application.
   *
   * @param args the command line arguments
   */
  public static void main(final String[] args) {
    ConfigurationUtil.init();
    LOG.info("Loaded properties are {}", ConfigurationUtil.getProperties());

    final String compileTopic = getProperty("mdp.kafka.compile.topic");
    final String executeTopic = getProperty("mdp.kafka.execute.topic");
    final String compilationTopic = getProperty("mdp.kafka.compilation.topic");
    final String executionTopic = getProperty("mdp.kafka.execution.topic");
    final String monitorTopic = getProperty("mdp.kafka.monitor.topic");
    final String tmpPath = getProperty("mdp.executor.tmp");
    final String socketPath = getProperty("mdp.docker.socket");
    final String kafkaHost = getProperty("mdp.kafka.host");
    final String zookeeperHost = getProperty("mdp.zookeeper.host");
    final String pluginDirectory = getProperty("mdp.executor.processors");
    final String threadCountString = getProperty("mdp.consumer.threads");
    final int threadCount = Integer.valueOf(threadCountString);
    final String clientId = UUID.randomUUID().toString();

    final ExecutorService executor = Executors.newFixedThreadPool(threadCount);
    final SimpleDockerClient dockerClient = createDockerClient(socketPath);

    final ProcessorFactory processorFactory = new ProcessorFactoryImpl(
        pluginDirectory);
    processorFactory.init();

    Producer kafkaProducer = createKafkaProducer(kafkaHost);

    final DataProducer monitorDataProducer
        = new DefaultDataProducer(monitorTopic, kafkaProducer);

    final ExecutorFactory executorFactory
        = new ExecutorFactoryImpl(tmpPath, dockerClient,
            processorFactory, monitorDataProducer, executor);

    final DataProducer compilationDataProducer
        = new DefaultDataProducer(compilationTopic, kafkaProducer);

    final SubmissionHandler compilationHandler
        = new CompilationHandler(executorFactory, compilationDataProducer);

    final SubmissionWorkerFactory compileWorkerFactory
        = new DefaultWorkerFactory(compilationHandler,
            CompileSubmissionData.class);

    ConsumerConnector compileConnector = createKafkaConsumer(clientId,
        zookeeperHost);
    final SubmissionConsumer compilationConsumer = new DefaultConsumer(
        compileTopic, compileConnector, compileWorkerFactory, executor);
    compilationConsumer.run(threadCount);

    final DataProducer executionDataProducer
        = new DefaultDataProducer(executionTopic, kafkaProducer);

    final SubmissionHandler executionHandler
        = new ExecutionHandler(executorFactory, executionDataProducer);

    final SubmissionWorkerFactory executeWorkerFactory
        = new DefaultWorkerFactory(executionHandler,
            ExecuteSubmissionData.class);

    ConsumerConnector executeConnector = createKafkaConsumer(clientId,
        zookeeperHost);
    final SubmissionConsumer executionConsumer = new DefaultConsumer(
        executeTopic, executeConnector, executeWorkerFactory, executor);
    executionConsumer.run(threadCount);

    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        LOG.info("Closing compilation consumer");
        compilationConsumer.shutdown();

        LOG.info("Closing execution consumer");
        executionConsumer.shutdown();
        LOG.info("Closing compilation data producer");
        compilationDataProducer.shutdown();

        LOG.info("Closing execution data producer");
        executionDataProducer.shutdown();
        LOG.info("Closing processor factory");
        processorFactory.shutdown();
      }
    });
  }

  /**
   * Creates Kafka producer instance.
   *
   * @param kafkaHost Kafka host with port number.
   * @return newly created Kafka producer instance.
   */
  private static Producer createKafkaProducer(final String kafkaHost) {
    Map<String, String> properties = new HashMap<>();
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaHost);
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
        JsonMessageSerializer.class.getCanonicalName());
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
        "org.apache.kafka.common.serialization.StringSerializer");
    properties.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");
    properties.put(ProducerConfig.ACKS_CONFIG, "1");
    properties.put(ProducerConfig.RETRIES_CONFIG, "3");
    return new KafkaProducer(properties);
  }

  /**
   * Creates Kafka connector instance.
   *
   * @param clientId      client id
   * @param zookeeperHost zookeeper host.
   * @return new created Kafka connector instance.
   */
  private static ConsumerConnector createKafkaConsumer(
      final String clientId, final String zookeeperHost) {
    Properties props = new Properties();
    props.put("zookeeper.connect", zookeeperHost);
    props.put("group.id", "mdp");
    props.put("client.id", clientId);
    props.put("zookeeper.session.timeout.ms", "400");
    props.put("zookeeper.sync.time.ms", "200");
    props.put("auto.commit.interval.ms", "1000");

    return Consumer.createJavaConsumerConnector(new ConsumerConfig(props));
  }

  /**
   * Creates instance of docker engine client, if client is not successfully
   * validation {@link Error} is thrown.
   *
   * @param socketPath path to UNIX socket of docker daemon.
   * @return newly created instance of docker client.
   */
  private static SimpleDockerClient createDockerClient(
      final String socketPath) {
    LOG.info("Creating instance of docker engine client...");
    final SimpleDockerClient dockerClient
        = new SimpleDockerClientImpl(socketPath);

    try {
      dockerClient.ping();
    } catch (DockerNotRunningException | DockerErrorException ex) {
      LOG.error("Unable to ping docker engine.", ex);
      throw new Error("Unable to create instance of docker client", ex);
    }
    LOG.info("Docker engine client successfully created a validated");

    return dockerClient;
  }
}
