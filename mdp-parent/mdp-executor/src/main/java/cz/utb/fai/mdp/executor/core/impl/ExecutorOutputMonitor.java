/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.core.impl;

import cz.utb.fai.mdp.executor.core.ExecutorEvent;
import cz.utb.fai.mdp.executor.core.ExecutorMonitor;
import cz.utb.fai.mdp.executor.kafka.DataProducer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link ExecutorMonitor}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class ExecutorOutputMonitor implements ExecutorMonitor {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(ExecutorOutputMonitor.class);

  /**
   * Instance of data producer for produces messages with event data.
   */
  private final DataProducer producer;

  /**
   * Creates instance of this listener.
   *
   * @param dataProducer data producer.
   */
  public ExecutorOutputMonitor(final DataProducer dataProducer) {
    this.producer = dataProducer;
  }

  @Override
  public void onEvent(final ExecutorEvent event) {
    LOG.debug("Handling event {}", event);
    producer.sendData(event.getData());
  }

}
