/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.core.impl;

import cz.utb.fai.mdp.commons.dto.CompilationResult;
import cz.utb.fai.mdp.commons.dto.ExecutionResult;
import cz.utb.fai.mdp.commons.dto.impl.CompilationResultImpl;
import cz.utb.fai.mdp.commons.dto.impl.ExecutionResultImpl;
import cz.utb.fai.mdp.commons.spi.Processor;
import cz.utb.fai.mdp.executor.core.AbstractExecutor;

import io.docker.model.ContainerConfig;
import io.docker.model.HostConfig;
import io.docker.model.StartConfig;

import java.io.File;
import java.util.ArrayList;

import static java.util.Arrays.asList;

import cz.utb.fai.mdp.commons.dto.MonitorSubmissionData;
import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.commons.dto.impl.MonitorSubmissionDataImpl;
import cz.utb.fai.mdp.executor.core.ExecutorEvent;
import cz.utb.fai.mdp.executor.core.ExecutorMonitor;

import org.apache.commons.io.IOUtils;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.docker.client.SimpleDockerClient;
import io.docker.exception.DockerErrorException;
import io.docker.filter.ListFilter;
import io.docker.filter.LogsFilter;
import io.docker.filter.impl.ListFilterBuilder;
import io.docker.filter.impl.LogsFilterBuilder;
import io.docker.model.Container;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Implementation of default executor based on Docker engine.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class DefaultExecutor extends AbstractExecutor {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(DefaultExecutor.class);

  /**
   * Working directory inside container.
   */
  private static final String WORK_DIR = "/sandbox";

  /**
   * Bash template command for execution commands inside container.
   */
  private static final String[] BASH_CMD = new String[]{"/bin/bash", "-c"};

  /**
   * Default docker image name.
   */
  private static final String DOCKER_IMAGE = "frantiseks/apac";

  /**
   * Max runtime of execution in seconds.
   */
  private static final int MAX_RUNTIME = 30;

  /**
   * Second represented in milliseconds.
   */
  private static final int MILLIS_SECOND = 1000;

  /**
   * Registered monitors.
   */
  private final Set<ExecutorMonitor> monitors = new HashSet<>();

  /**
   * Holds used container ids.
   */
  private final List<String> containerIds = new ArrayList<>();
  private final ExecutorService executor;
  private final SimpleDockerClient client;

  private String dockerImage = DOCKER_IMAGE;

  public DefaultExecutor(SimpleDockerClient client, Processor processor,
      ExecutorService executorService, File workspace) {
    super(workspace, processor);
    this.client = client;
    this.executor = executorService;
  }

  /**
   * Overrides default sandbox container image.
   *
   * @param imageName docker image name
   * @throws IllegalArgumentException if dockerImage {@code null} or empty.
   */
  public void setDockerImage(final String imageName) {
    if (imageName == null || imageName.isEmpty()) {
      throw new IllegalArgumentException("imageName must be "
          + "non-empty string");
    }
    this.dockerImage = imageName;
  }

  @Override
  public CompilationResult compile() {
    String compileOutput = "";

    String compileCommand = processor.buildCompileCommand(workspace);

    if (compileCommand == null || compileCommand.isEmpty()) {
      throw new IllegalStateException("Unexpected compile command");
    }

    ContainerConfig config = createConfig(workspace.getName(),
        compileCommand, false);

    try {
      String containerId = client.create(config).getId();
      startContainer(containerId);

      LogsFilter filter = new LogsFilterBuilder()
          .setStderr(true)
          .setStdout(true)
          .setFollow(true)
          .build();
      compileOutput = IOUtils.toString(client.getLogs(containerId, filter));

    } catch (DockerErrorException ex) {
      LOG.error("Compile execution error for workspace {}",
          workspace.getName(), ex);
    } catch (IOException ex) {
      LOG.error("Unable to read compile out", ex);
    }

    CompilationResult result = new CompilationResultImpl(compileOutput, false);
    boolean isCompilationSuccess = processor.isCompilationSuccess(result);

    return new CompilationResultImpl(compileOutput, !isCompilationSuccess);
  }

  @Override
  public ExecutionResult execute(final String arguments) {
    String executeCommand = processor.buildExecuteCommand(workspace);
    executeCommand += " " + arguments;

    if (executeCommand == null || executeCommand.isEmpty()) {
      throw new IllegalStateException("Unexpected execute command");
    }

    String executionOutput = "";
    long duration = 0;

    ContainerConfig config = createConfig(workspace.getName(),
        executeCommand, true);

    final StringBuilder outputBuilder = new StringBuilder();
    monitors.add(new LogMonitor(outputBuilder));

    boolean jobEnded = false;
    try {
      String containerId = client.create(config).getId();
      startContainer(containerId);

      long startTime = System.currentTimeMillis();
      long endTime = System.currentTimeMillis();

      executor.submit(new LogReader(containerId, containerId))
          .get(MAX_RUNTIME, TimeUnit.SECONDS);

      ListFilter filter = new ListFilterBuilder().build();

      while (endTime - startTime < (MAX_RUNTIME * MILLIS_SECOND)) {
        Optional<Container> exists = client.get(filter).stream()
            .filter(c -> c.getId().equals(containerId))
            .findFirst();

        if (!exists.isPresent()) {
          LOG.info("Container {} for workspace UUID {} ended job",
              containerId, workspace.getName());
          jobEnded = true;
          break;
        } else {
          LOG.debug("Container {} for workspace UUID {} still processing job",
              containerId, workspace.getName());
        }

        endTime = System.currentTimeMillis();
      }

      if (!jobEnded) {
        client.kill(containerId);

        LOG.info("Container {} for workspace UUID {} has been killed",
            containerId, workspace.getName());
      }

      executionOutput = outputBuilder.toString();

    } catch (DockerErrorException ex) {
      LOG.error("Compile execution error for workspace {}", workspace.getName(),
          ex);
    } catch (InterruptedException | ExecutionException | TimeoutException ex) {
      LOG.error("Reader thread exception", ex);
    }
    ExecutionResult result = new ExecutionResultImpl(executionOutput,
        duration, !jobEnded);

    return result;
  }

  @Override
  public void cleanUp() {
    try {
      for (String id : containerIds) {
        LOG.debug("Deleting container {}", id);
        client.delete(id);
      }
    } catch (DockerErrorException ex) {
      LOG.error("CleanUp execution error for workspace {}",
          workspace.getName(), ex);
    }

    if (executor != null) {
      executor.shutdown();
    }
  }

  @Override
  public void addMonitor(final ExecutorMonitor monitor) {
    if (monitor == null) {
      throw new IllegalArgumentException("Monitor cannot be null");
    }
    monitors.add(monitor);
  }

  private ContainerConfig createConfig(String workspaceUuid, String command,
      boolean exeContainer) {
    HashMap<String, ?> volumeMap = new HashMap<>();
    volumeMap.put(WORK_DIR, null);
    ContainerConfig config = new ContainerConfig.Builder()
        .setImage(dockerImage)
        .setHostName(workspaceUuid)
        .setTty(true)
        .setVolumes(volumeMap)
        .setWorkingDir(WORK_DIR)
        .build();

    if (exeContainer) {
      HostConfig hostConfig = new HostConfig.Builder()
          .build();
      config.setHostConfig(hostConfig);
    }

    List<String> cmdWithArgs = new ArrayList<>(asList(BASH_CMD));
    cmdWithArgs.add(command);

    String[] cmd = new String[3];
    config.setCmd(cmdWithArgs.toArray(cmd));

    return config;
  }

  private void startContainer(String containerId) {
    StartConfig startConfig = new StartConfig();
    startConfig.addBind(workspace.getAbsolutePath(), WORK_DIR, true);
    client.start(containerId, startConfig);
    containerIds.add(containerId);
  }

  private void notifyOutputChange(String uuid, String output) {
    monitors.forEach((monitor) -> {
      monitor.onEvent(createExecutorOutputEvent(uuid, output));
    });
  }

  private ExecutorEvent createExecutorOutputEvent(final String uuid,
      final String outputChange) {
    SubmissionData data = new MonitorSubmissionDataImpl(uuid, outputChange);
    ExecutorEvent event = new OutputExecutorEvent(data);
    return event;
  }

  /**
   * Asynchronous log reader which produces output change event to registered
   * monitors.
   */
  private class LogReader implements Runnable {

    /**
     * Submission UUID.
     */
    private final String uuid;

    /**
     * Id of sandbox container.
     */
    private final String containerId;

    /**
     * Creates instance of log reader.
     *
     * @param submissionUuid submission UUID
     * @param sandboxId      sandbox container id
     */
    public LogReader(final String submissionUuid, final String sandboxId) {
      this.uuid = submissionUuid;
      this.containerId = sandboxId;
    }

    @Override
    public void run() {
      try {
        LogsFilter filter = new LogsFilterBuilder()
            .setStderr(true)
            .setStdout(true)
            .setFollow(true)
            .build();

        InputStream logStream = client.getLogs(uuid, filter);

        Scanner scanner = new Scanner(logStream);
        while (scanner.hasNextLine()) {
          notifyOutputChange(uuid, scanner.nextLine());
        }
      } catch (DockerErrorException ex) {
        LOG.error("Unable to get current output from container {}",
            containerId, ex);
      }
    }
  }

  /**
   * Asynchronous log monitor collect execution output.
   */
  private class LogMonitor implements ExecutorMonitor {

    /**
     * Holds collected output.
     */
    private final StringBuilder outputBuilder;

    /**
     * Creates new instance of log monitor.
     *
     * @param builder instance of string builder
     */
    public LogMonitor(final StringBuilder builder) {
      this.outputBuilder = builder;
    }

    @Override
    public void onEvent(final ExecutorEvent event) {
      if (event instanceof OutputExecutorEvent) {
        MonitorSubmissionData data = (MonitorSubmissionData) event.getData();
        outputBuilder.append(data.getOutputChange());
      }
    }

  }
}
