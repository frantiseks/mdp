/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.core.impl;

import cz.utb.fai.mdp.commons.enums.Language;
import cz.utb.fai.mdp.commons.spi.Processor;
import cz.utb.fai.mdp.executor.core.Executor;
import cz.utb.fai.mdp.executor.core.ExecutorFactory;
import cz.utb.fai.mdp.executor.core.ProcessorFactory;
import cz.utb.fai.mdp.executor.kafka.DataProducer;

import io.docker.client.SimpleDockerClient;

import java.io.File;
import java.util.concurrent.ExecutorService;

/**
 * Implementation of {@link ExecutorFactory}. Implementation creates default
 * executor with register monitor of output changes.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class ExecutorFactoryImpl implements ExecutorFactory {

  /**
   * Base path to MDP workspaces.
   */
  private final String basePath;

  /**
   * Instance of docker client.
   */
  private final SimpleDockerClient client;

  /**
   * Instance of submission processor factory.
   */
  private final ProcessorFactory factory;

  /**
   * Instance of data producer for produces messages with output changes.
   */
  private final DataProducer producer;

  /**
   * Executor thread pool service.
   */
  private final ExecutorService executorService;

  /**
   * Creates instance of executor factory.
   *
   * @param baseWorkspacePath base path to MDP workspaces
   * @param dockerClient      docker client
   * @param processorFactory  submission processor factory
   * @param monitorProducer   data producer for output changes.
   * @param executor          instance of executor service
   */
  public ExecutorFactoryImpl(final String baseWorkspacePath,
      final SimpleDockerClient dockerClient,
      final ProcessorFactory processorFactory,
      final DataProducer monitorProducer,
      final ExecutorService executor) {
    this.basePath = baseWorkspacePath;
    this.client = dockerClient;
    this.factory = processorFactory;
    this.producer = monitorProducer;
    this.executorService = executor;
  }

  @Override
  public Executor makeExecutor(final String submissionUuid,
      final Language language) {
    File workspace = new File(basePath + File.separator + submissionUuid);

    Processor processor = factory
        .makeProcessor(language);

    Executor executor = new DefaultExecutor(client, processor,
        executorService, workspace);

    executor.addMonitor(new ExecutorOutputMonitor(producer));

    return executor;
  }

}
