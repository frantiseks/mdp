/*
 *
 */

package cz.utb.fai.mdp.executor.kafka.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.utb.fai.mdp.commons.dto.SubmissionData;

import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * JSON value serializer for Kafka.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class JsonMessageSerializer implements Serializer<SubmissionData> {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(JsonMessageSerializer.class);

  /**
   * Instance of Jackson JSON serializer and deserializer.
   */
  private final ObjectMapper mapper = new ObjectMapper();

  @Override
  public void configure(final Map<String, ?> map, final boolean bln) {
  }

  @Override
  public byte[] serialize(final String string, final SubmissionData data) {
    byte[] bytes = new byte[0];
    try {
      bytes = mapper.writeValueAsBytes(data);
    } catch (JsonProcessingException ex) {
      LOG.error("Unable to serialize input data", ex);
    }
    return bytes;
  }

  @Override
  public void close() {
  }
}
