/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.service;

import static cz.utb.fai.mdp.executor.util.ConfigurationUtil.getProperty;

import cz.utb.fai.mdp.commons.dto.CompilationResult;
import cz.utb.fai.mdp.commons.dto.CompileSubmissionData;
import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.commons.dto.impl.CompilationSubmissionDataImpl;
import cz.utb.fai.mdp.executor.core.Executor;
import cz.utb.fai.mdp.executor.core.ExecutorFactory;
import cz.utb.fai.mdp.executor.kafka.DataProducer;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Implementation of {@link SubmissionHandler}. Its implementation for
 * processing submission compilation request.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class CompilationHandler implements SubmissionHandler {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(CompilationHandler.class);

  private static final String DATE_EXTENSION = ".data";

  private String workspacePath;
  private final ExecutorFactory executorFactory;
  private final DataProducer producer;

  public CompilationHandler(ExecutorFactory executorFactory,
      DataProducer producer) {
    this.workspacePath = getProperty("mdp.executor.tmp");
    this.executorFactory = executorFactory;
    this.producer = producer;
  }

  public final void setWorkspacePath(final String workspacePath) {
    this.workspacePath = workspacePath;
  }

  @Override
  public void handle(final SubmissionData data) {
    if (!(data instanceof CompileSubmissionData)) {
      return;
    }
    CompileSubmissionData compileData = (CompileSubmissionData) data;

    File workspace = createWorkspace(data.getUuid());

    String submissionFilePath = new StringBuilder()
        .append(workspace.getAbsolutePath())
        .append(File.separator)
        .append(System.currentTimeMillis())
        .append(DATE_EXTENSION).toString();

    File submissionFile = new File(submissionFilePath);
    try {
      FileUtils.copyURLToFile(compileData.getDownloadUrl(), submissionFile);

      ZipUtil.unpack(submissionFile, workspace);
    } catch (MalformedURLException ex) {
      LOG.error("Unable to process submission data", ex);
    } catch (IOException ex) {
      LOG.error("Unable to process submission data", ex);
    }

    Executor executor = executorFactory
        .makeExecutor(data.getUuid(), compileData.getLanguage());

    CompilationResult result = executor.compile();

    SubmissionData compilationData
        = new CompilationSubmissionDataImpl(data.getUuid(),
            result);

    producer.sendData(compilationData);

    executor.cleanUp();
  }

  private File createWorkspace(String uuid) {
    String path = workspacePath + File.separator + uuid;

    File workspace = new File(path);
    workspace.mkdir();

    return workspace;
  }
}
