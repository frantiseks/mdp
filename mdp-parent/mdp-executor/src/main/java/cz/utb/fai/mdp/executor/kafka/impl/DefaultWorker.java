/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.kafka.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.mrbean.MrBeanModule;

import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.executor.kafka.SubmissionWorker;
import cz.utb.fai.mdp.executor.service.SubmissionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.message.MessageAndMetadata;

/**
 * Default implementation of {@link SubmissionWorker}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class DefaultWorker implements SubmissionWorker {

  /**
   * Instance of logger for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(DefaultWorker.class);

  /**
   * Kafka message stream.
   */
  private final KafkaStream messageStream;

  /**
   * Worker id number.
   */
  private final int threadNumber;

  /**
   * Submission data handler.
   */
  private final SubmissionHandler submissionHandler;
  private final ObjectMapper mapper;

  /**
   * Class which is used to deserialize submission data.
   */
  private final Class<? extends SubmissionData> deserializeClass;

  /**
   * Creates instance of default worker.
   *
   * @param stream   Kafka message stream.
   * @param workerId worker number
   * @param handler  handler for submission data
   * @param clazz    class for deserialization of submission data.
   */
  public DefaultWorker(
      final KafkaStream stream,
      final int workerId,
      final SubmissionHandler handler,
      final Class<? extends SubmissionData> clazz) {
    this.messageStream = stream;
    this.threadNumber = workerId;
    this.submissionHandler = handler;
    this.deserializeClass = clazz;

    this.mapper = new ObjectMapper();
    mapper.registerModule(new MrBeanModule());
  }

  @Override
  public void run() {
    LOG.debug("Running worker number {}", threadNumber);
    ConsumerIterator<String, byte[]> streamIt = messageStream.iterator();
    LOG.debug("Stream iterator aquired for client id {}", streamIt.clientId());
    while (streamIt.hasNext()) {
      try {
        MessageAndMetadata<String, byte[]> message = streamIt.next();
        LOG.debug("Message with key {} is processed by thread {}",
            message.key(), threadNumber);

        SubmissionData data = mapper.readValue((byte[]) message.message(),
            deserializeClass);

        LOG.info("Processing submission data with uuid {}", data.getUuid());
        submissionHandler.handle(data);
      } catch (Exception ex) {
        LOG.error("Unable to process message", ex);
      }
    }
  }
}
