
package cz.utb.fai.mdp.executor.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to handle executor configuration.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class ConfigurationUtil {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOGGER = LoggerFactory
      .getLogger(ConfigurationUtil.class);

  /**
   * Default configuration file name.
   */
  private static final String DEFAULT_CONFIG_FILE = "config.properties";

  /**
   * Holds loaded properties.
   */
  private static Properties properties;

  private ConfigurationUtil() {

  }

  /**
   * Loads properties from file in static way.
   */
  public static void init() {
    try {
      InputStream inputStream = Thread.currentThread()
          .getContextClassLoader()
          .getResourceAsStream(DEFAULT_CONFIG_FILE);
      properties = new Properties();
      properties.load(inputStream);
    } catch (IOException ex) {
      LOGGER.error("Unable to load properties from file", ex);
    }
  }

  /**
   * Loads properties from specified path. Should be used only in tests.
   *
   * @param configPath absolute path to property file
   */
  public static void initDebug(String configPath) {
    try {
      InputStream inputStream = new FileInputStream(configPath);
      properties = new Properties();
      properties.load(inputStream);
    } catch (IOException ex) {
      LOGGER.error("Unable to load properties from file ", ex);
    }
  }

  /**
   * Returns all loaded properties.
   *
   * @return loaded properties
   */
  public static Properties getProperties() {
    return properties;
  }

  /**
   * Gets property by key.
   *
   * @param key property lookup key
   * @return if key was found property value, otherwise empty string
   */
  public static String getProperty(String key) {
    return properties.containsKey(key) ? properties.getProperty(key) : "";
  }
}
