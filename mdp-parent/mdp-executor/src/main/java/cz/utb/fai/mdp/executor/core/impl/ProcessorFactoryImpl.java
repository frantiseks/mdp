/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.core.impl;

import static java.util.Arrays.asList;
import static java.util.ServiceLoader.load;

import cz.utb.fai.mdp.commons.enums.Language;
import cz.utb.fai.mdp.commons.spi.Processor;
import cz.utb.fai.mdp.commons.spi.ProcessorInfo;
import cz.utb.fai.mdp.commons.util.GenericExtFilter;
import cz.utb.fai.mdp.executor.core.ProcessorFactory;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * Implementation of {@link ProcessorFactory}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class ProcessorFactoryImpl implements ProcessorFactory {

  /**
   * Instance of logger for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(ProcessorFactoryImpl.class);

  /**
   * Store instance which points to plugin directory.
   */
  private final File pluginsDir;

  /**
   * Holds newly loaded plugin classes.
   */
  private ServiceLoader<Processor> loader;

  /**
   * Watchs directory with plug-ins for changes.
   */
  private FileAlterationMonitor monitor;

  /**
   * Creates instance with specified path to directory with plug-ins.
   *
   * @param pluginsDirectory path to directory with plug-ins, if do not exists
   *                         new one is created.
   */
  public ProcessorFactoryImpl(final String pluginsDirectory) {
    this.pluginsDir = new File(pluginsDirectory);
    this.pluginsDir.mkdir();
  }

  @Override
  public void init() {
    loadPlugins(pluginsDir);

    FileAlterationObserver directoryObserver
        = new FileAlterationObserver(pluginsDir);
    directoryObserver.addListener(new FileAlterationListenerImpl());

    monitor = new FileAlterationMonitor();
    monitor.addObserver(directoryObserver);

    try {
      monitor.start();
    } catch (Exception ex) {
      LOG.error("Unable to start directory monitor", ex);
    }
  }

  @Override
  public void shutdown() {
    try {
      monitor.stop();
    } catch (Exception ex) {
      LOG.error("Unable to stop directory monitor", ex);
    }
  }

  @Override
  public Processor makeProcessor(final Language language) {
    for (Processor subProcessor : loader) {
      if (subProcessor.getClass().isAnnotationPresent(ProcessorInfo.class)) {

        Annotation annotation = subProcessor.getClass()
            .getAnnotation(ProcessorInfo.class);

        ProcessorInfo processorInfo = (ProcessorInfo) annotation;

        if (processorInfo.language() == language
            && processorInfo.version() >= 1) {
          return subProcessor;
        }
      }
    }

    return null;
  }

  /**
   * Loads submission processors JAR files from specified directory.
   *
   * @param processorsDir directory with JAR files.
   */
  private void loadPlugins(final File processorsDir) {
    if (processorsDir == null) {
      throw new IllegalArgumentException("ProcessorsDir cannot be null");
    }
    LOG.info("Loading submission processors from directory {} ",
        processorsDir.getAbsolutePath());

    List<File> jars = asList(processorsDir
        .listFiles(new GenericExtFilter(new String[]{"jar"})));

    if (jars == null) {
      LOG.info("No plugins found");
      return;
    }

    List<URL> jarUrls = new ArrayList<>();
    try {
      for (File f : jars) {
        jarUrls.add(f.toURI().toURL());
      }
    } catch (MalformedURLException ex) {
      LOG.error("Invalid JAR file URL", ex);
    }

    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    //load external class to current runtime
    URLClassLoader jarClassLoader = new URLClassLoader(jarUrls
        .toArray(new URL[jarUrls.size()]), classLoader);

    loader = load(Processor.class, jarClassLoader);

    loader.forEach(p -> LOG.debug("Plugin {} loaded.", p.getClass()
        .getSimpleName()));
  }

  /**
   * Private implementation of {@link FileAlterationListener} to watch changes
   * in the plug-in directory.
   */
  private class FileAlterationListenerImpl implements FileAlterationListener {

    @Override
    public void onStart(final FileAlterationObserver fao) {
      LOG.debug("Starting directory monitoring");
    }

    @Override
    public void onDirectoryCreate(final File file) {
      LOG.debug("Plugin directory change");
    }

    @Override
    public void onDirectoryChange(final File file) {
      LOG.debug("Plugin directory change");
    }

    @Override
    public void onDirectoryDelete(final File file) {
      LOG.debug("Plugin directory change");
    }

    @Override
    public void onFileCreate(final File file) {
      LOG.debug("New plugin found, reloading");
      loadPlugins(file.getParentFile());
    }

    @Override
    public void onFileChange(final File file) {
      LOG.debug("Plugin change was detected");
      loadPlugins(file.getParentFile());
    }

    @Override
    public void onFileDelete(final File file) {
      LOG.debug("Plugin has been deleted, reloading");
      loadPlugins(file.getParentFile());
    }

    @Override
    public void onStop(final FileAlterationObserver fao) {
      LOG.debug("Stopping directory monitoring");
    }
  }
}
