/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.executor.core;

import cz.utb.fai.mdp.commons.spi.Processor;

import java.io.File;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public abstract class AbstractExecutor implements Executor {

  protected final File workspace;

  protected final Processor processor;

  public AbstractExecutor(File workspace, Processor processor) {
    this.workspace = workspace;
    this.processor = processor;
  }

}
