/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.api;

import static cz.utb.fai.mdp.commons.util.WorkspaceUtil.workspacePath;
import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.post;

import cz.utb.fai.mdp.commons.enums.Extension;
import cz.utb.fai.mdp.server.domain.Submission;
import cz.utb.fai.mdp.server.service.SubmissionService;
import cz.utb.fai.mdp.server.util.ConfigurationUtil;
import cz.utb.fai.mdp.server.util.UploadUtil;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Optional;

/**
 * Internal resource to handle submission distribution to executors and upload
 * compiled binaries.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class InternalSubmissionResource {

  private static final Logger LOG = LoggerFactory
      .getLogger(InternalSubmissionResource.class);

  private static final String CONTEXT = "/mdp/internal";
  private static final String OCTET_STREAM = "application/octet-stream";
  private static final String ZIP_EXTENSION = ".zip";

  private final SubmissionService submissionService;

  private String basePath;

  public InternalSubmissionResource(SubmissionService submissionService) {
    setupEndpoints();
    this.basePath = ConfigurationUtil.getProperty("mdp.server.basePath");
    this.submissionService = submissionService;
  }

  public void setBasePath(String basePath) {
    if (basePath == null || basePath.isEmpty()) {
      throw new IllegalArgumentException("BasePath must be non-empty string");
    }
    this.basePath = basePath;
  }

  private void setupEndpoints() {
    String uploadPath = ConfigurationUtil.getProperty("mdp.server.upload");

    get(CONTEXT + "/submissions/:uuid/download", (request, response) -> {
      String uuid = request.params(":uuid");

      Optional<Submission> opSubmission = submissionService.get(uuid);
      if (!opSubmission.isPresent()) {
        LOG.info("Submission with uuid {} not found", uuid);
        halt(404);
      }

      File zipFile = getSubmissionZipFile(uuid);

      if (!zipFile.exists()) {
        LOG.error("Submission zip file not found on path",
            zipFile.getAbsolutePath());
        halt(404);
      }

      response.type(OCTET_STREAM);
      return FileUtils.readFileToByteArray(zipFile);
    });

    get(CONTEXT + "/submissions/:uuid/binary", (request, response) -> {
      String uuid = request.params(":uuid");

      Optional<Submission> opSubmission = submissionService.get(uuid);
      if (!opSubmission.isPresent()) {
        LOG.info("Submission with uuid {} not found", uuid);
        halt(404);
      }

      Submission submission = opSubmission.get();
      if (!submission.isAlreadyCompiled()) {
        LOG.info("Compiled submission with uuid {} not found", uuid);
        halt(417);
      }

      File compiledBinary = getCompiledBinary(uuid);

      if (!compiledBinary.exists()) {
        LOG.error("Submission compiled binary not found on path",
            compiledBinary.getAbsolutePath());
        halt(404);
      }

      response.type(OCTET_STREAM);
      return FileUtils.readFileToByteArray(compiledBinary);
    });

    post(CONTEXT + "/submissions/:uuid/upload", OCTET_STREAM,
        (request, response) -> {
          final byte[] binaryFileBytes = request.bodyAsBytes();

          if (binaryFileBytes.length == 0) {
            halt(400);
          }

          UploadUtil.processFileUpload(binaryFileBytes, uploadPath);

          return response;
        }, new JsonTransformer());
  }

  private File getSubmissionZipFile(String uuid) {
    String workspacePath = workspacePath(basePath, uuid);

    String zipFilePath = workspacePath + File.separator
        + uuid + ZIP_EXTENSION;

    return new File(zipFilePath);
  }

  private File getCompiledBinary(String uuid) {
    String workspacePath = workspacePath(basePath, uuid);

    String binaryFilePath = workspacePath + File.separator
        + uuid + Extension.BUILD.toString();

    return new File(binaryFilePath);
  }
}
