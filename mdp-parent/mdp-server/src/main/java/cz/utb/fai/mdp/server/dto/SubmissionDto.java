/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import cz.utb.fai.mdp.commons.enums.Language;
import cz.utb.fai.mdp.server.domain.Submission;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class SubmissionDto {

  private String uuid;
  private Language language;
  private Boolean compilationFailed;
  private Boolean executionTerminated;
  private String compilationOutput;
  private String executionOutput;

  public SubmissionDto() {
  }

  public SubmissionDto(String uuid) {
    this.uuid = uuid;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public Language getLanguage() {
    return language;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  public Boolean getCompilationFailed() {
    return compilationFailed;
  }

  public void setCompilationFailed(Boolean compilationFailed) {
    this.compilationFailed = compilationFailed;
  }

  public Boolean getExecutionTerminated() {
    return executionTerminated;
  }

  public void setExecutionTerminated(Boolean executionTerminated) {
    this.executionTerminated = executionTerminated;
  }

  public String getCompilationOutput() {
    return compilationOutput;
  }

  public void setCompilationOutput(String compilationOutput) {
    this.compilationOutput = compilationOutput;
  }

  public String getExecutionOutput() {
    return executionOutput;
  }

  public void setExecutionOutput(String executionOutput) {
    this.executionOutput = executionOutput;
  }

  public static SubmissionDto from(Submission submission) {
    SubmissionDto dto = new SubmissionDto();
    dto.setCompilationFailed(submission.isCompilationFailed());
    dto.setExecutionTerminated(submission.isExecutionTerminated());
    dto.setExecutionOutput(submission.getExecutionOutput());
    dto.setCompilationOutput(submission.getCompilationOutput());
    dto.setUuid(submission.getUuid());
    dto.setLanguage(Language.fromCode(submission.getLanguage()));
    return dto;
  }

}
