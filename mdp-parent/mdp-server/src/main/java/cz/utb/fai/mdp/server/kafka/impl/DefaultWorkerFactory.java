/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.kafka.impl;

import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.server.kafka.DataWorker;
import cz.utb.fai.mdp.server.kafka.DataWorkerFactory;
import cz.utb.fai.mdp.server.service.DataHandler;

import kafka.consumer.KafkaStream;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class DefaultWorkerFactory implements DataWorkerFactory {

  private final DataHandler handler;
  private final Class<? extends SubmissionData> deserializeClass;

  public DefaultWorkerFactory(final DataHandler handler,
      final Class<? extends SubmissionData> deserializeClass) {
    this.handler = handler;
    this.deserializeClass = deserializeClass;
  }

  @Override
  public DataWorker makeWorker(KafkaStream stream, int threadNumber) {
    return new DefaultWorker(stream, threadNumber, handler, deserializeClass);
  }

}
