/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.dao.impl;

import cz.utb.fai.mdp.commons.dto.CompilationResult;
import cz.utb.fai.mdp.commons.dto.ExecutionResult;
import cz.utb.fai.mdp.server.dao.SubmissionDao;
import cz.utb.fai.mdp.server.domain.Submission;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.sql.DataSource;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class SubmissionDaoImpl implements SubmissionDao {

  private static final Logger LOG = LoggerFactory
      .getLogger(SubmissionDaoImpl.class);

  private final Sql2o sql2o;

  private static final String INSERT_QUERY = "INSERT "
      + "INTO submission(language, uuid) "
      + "VALUES(:language, RANDOM_UUID())";

  private static final String SELECT_ONE_QUERY = "SELECT * "
      + "FROM submission WHERE uuid = :uuid";

  private static final String SELECT_ONE_BY_ID_QUERY = "SELECT "
      + "uuid FROM submission WHERE id = :id";

  private static final String UPDATE_PARTITION_QUERY = "UPDATE submission "
      + "SET kafa_partition = :partition WHERE uuid = :uuid";

  private static final String SET_COMPILATION_RESULT_QUERY = "UPDATE submission "
      + "SET compilation_output = :compilationOutput, "
      + "compilation_failed = :compilationFailed, "
      + "already_compiled = :alreadyCompiled "
      + "WHERE uuid = :uuid";

  private static final String SET_EXECUTION_RESULT_QUERY = "UPDATE submission "
      + "SET execution_output = :executionOutput, "
      + "execution_terminated = :executionTerminated "
      + "WHERE uuid = :uuid";

  public SubmissionDaoImpl(DataSource dataSource) {
    this.sql2o = new Sql2o(dataSource);
    this.sql2o.setDefaultColumnMappings(columnMappings());
  }

  @Override
  public List<Submission> findAll() {
    return new ArrayList<>();
  }

  @Override
  public Optional<Submission> findOne(final String uuid) {
    try (Connection con = sql2o.open()) {
      return Optional.ofNullable(con.createQuery(SELECT_ONE_QUERY)
          .addParameter("uuid", uuid)
          .executeAndFetchFirst(Submission.class));
    }
  }

  @Override
  public String insert(final Submission submission) {
    try (Connection con = sql2o.open()) {
      Integer id = con.createQuery(INSERT_QUERY)
          .bind(submission)
          .executeUpdate()
          .getKey(Integer.class);

      return getById(id);
    }
  }

  @Override
  public void setCompilationResult(final String uuid,
      final CompilationResult compilationResult) {
    try (Connection con = sql2o.open()) {
      con.createQuery(SET_COMPILATION_RESULT_QUERY)
          .addParameter("compilationOutput", compilationResult.getOutput())
          .addParameter("compilationFailed", compilationResult.isFailed())
          .addParameter("alreadyCompiled", !compilationResult.isFailed())
          .addParameter("uuid", uuid)
          .executeUpdate();
    }
  }

  @Override
  public void setExecutionResult(final String uuid,
      final ExecutionResult executionResult) {
    try (Connection con = sql2o.open()) {
      con.createQuery(SET_EXECUTION_RESULT_QUERY)
          .addParameter("executionOutput", executionResult.getOutput())
          .addParameter("executionTerminated", executionResult.isTerminated())
          .addParameter("uuid", uuid)
          .executeUpdate();
    }
  }

  @Override
  public void updatePartition(final String uuid, final int partition) {
    try (Connection con = sql2o.open()) {
      con.createQuery(UPDATE_PARTITION_QUERY)
          .addParameter("uuid", uuid)
          .addParameter("partition", partition)
          .executeUpdate();
    }
  }

  private String getById(Integer id) {
    try (Connection con = sql2o.open()) {
      return con.createQuery(SELECT_ONE_BY_ID_QUERY)
          .addParameter("id", id)
          .executeAndFetchFirst(String.class);
    }
  }

  private Map<String, String> columnMappings() {
    Map<String, String> columnMap = new HashMap<>();
    columnMap.put("compilation_failed", "compilationFailed");
    columnMap.put("compilation_output", "compilationOutput");
    columnMap.put("already_compiled", "alreadyCompiled");
    columnMap.put("execution_terminated", "executionTerminated");
    columnMap.put("execution_output", "executionOutput");
    columnMap.put("kafka_partition", "kafkaPartition");
    return columnMap;
  }
}
