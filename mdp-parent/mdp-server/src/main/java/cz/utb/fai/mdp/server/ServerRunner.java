/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server;

import static cz.utb.fai.mdp.server.util.ConfigurationUtil.getProperty;
import static spark.SparkBase.port;

import cz.utb.fai.mdp.commons.dto.CompilationSubmissionData;
import cz.utb.fai.mdp.commons.dto.ExecutionSubmissionData;
import cz.utb.fai.mdp.commons.dto.MonitorSubmissionData;
import cz.utb.fai.mdp.server.api.InternalSubmissionResource;
import cz.utb.fai.mdp.server.api.SubmissionResource;
import cz.utb.fai.mdp.server.dao.SubmissionDao;
import cz.utb.fai.mdp.server.dao.impl.SubmissionDaoImpl;
import cz.utb.fai.mdp.server.kafka.DataConsumer;
import cz.utb.fai.mdp.server.kafka.DataWorkerFactory;
import cz.utb.fai.mdp.server.kafka.SubmissionProducer;
import cz.utb.fai.mdp.server.kafka.impl.DefaultConsumer;
import cz.utb.fai.mdp.server.kafka.impl.DefaultWorkerFactory;
import cz.utb.fai.mdp.server.kafka.impl.DefaultSubmissionProducer;
import cz.utb.fai.mdp.server.service.DataHandler;
import cz.utb.fai.mdp.server.service.SubmissionService;
import cz.utb.fai.mdp.server.service.impl.CompilationDataHandler;
import cz.utb.fai.mdp.server.service.impl.ExecutionDataHandler;
import cz.utb.fai.mdp.server.service.impl.MonitorDataHandler;
import cz.utb.fai.mdp.server.service.impl.SubmissionServiceImpl;
import cz.utb.fai.mdp.server.util.ConfigurationUtil;
import cz.utb.fai.mdp.server.util.DatabaseUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.javaapi.consumer.ConsumerConnector;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Bootstrap class for MDP server application.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class ServerRunner {

  /**
   * Private constructor of runner class.
   */
  private ServerRunner() {
  }

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory.getLogger(ServerRunner.class);

  /**
   * Main method of MDP server application.
   *
   * @param args the command line arguments
   */
  public static void main(final String[] args) {
    try {
      ConfigurationUtil.init();
      String compilationDataTopic = getProperty("mdp.kafka.compilation.topic");
      String executionDataTopic = getProperty("mdp.kafka.execution.topic");
      String monitorTopic = getProperty("mdp.kafka.monitor.topic");
      String zookeeperHost = getProperty("mdp.zookeeper.host");
      int serverPort = Integer.valueOf(getProperty("mdp.server.port"));
      port(serverPort);

      final String threadCountString = getProperty("mdp.consumer.threads");
      final int threadCount = Integer.valueOf(threadCountString);

      LOG.info("MDP server starting...");

      LOG.info("Initializing configuration...");
      ConfigurationUtil.init();
      LOG.debug("Initialized configuration {}",
          ConfigurationUtil.getProperties());

      LOG.info("Initializing local database...");
      DatabaseUtil.init();
      LOG.debug("Initialized database {}", DatabaseUtil.getDataSource());

      LOG.info("Creating database schema...");
      DatabaseUtil.createSchema();
      LOG.info("Database schema created");

      final SubmissionDao submissionDao
          = new SubmissionDaoImpl(DatabaseUtil.getDataSource());

      LOG.info("Creating Kafka producer...");
      final SubmissionProducer submissionProducer
          = new DefaultSubmissionProducer();
      LOG.info("Producer created");

      String localIpAddress = InetAddress.getLocalHost().getHostAddress();
      LOG.info("Local ip address of server is {}", localIpAddress);

      final SubmissionService submissionService
          = new SubmissionServiceImpl(submissionDao, submissionProducer,
              localIpAddress + ":" + serverPort);

      final DataHandler dataHandler = new CompilationDataHandler(submissionDao);
      final DataWorkerFactory compilationWorkerFactory
          = new DefaultWorkerFactory(dataHandler,
              CompilationSubmissionData.class);

      final String clientId = UUID.randomUUID().toString();
      final ConsumerConnector compilationKafkaConnector
          = createKafkaConsumer(clientId, zookeeperHost);

      final ExecutorService executor = Executors.newCachedThreadPool();

      final DataConsumer compilationDataConsumer
          = new DefaultConsumer(compilationDataTopic,
              compilationKafkaConnector, compilationWorkerFactory, executor);
      compilationDataConsumer.run(threadCount);

      final DataHandler executionDataHandler
          = new ExecutionDataHandler(submissionDao);

      final DataWorkerFactory executionWorkerFactory
          = new DefaultWorkerFactory(executionDataHandler,
              ExecutionSubmissionData.class);

      final ConsumerConnector executionKafkaConnector
          = createKafkaConsumer(clientId, zookeeperHost);
      final DataConsumer executionDataConsumer
          = new DefaultConsumer(executionDataTopic, executionKafkaConnector,
              executionWorkerFactory, executor);
      executionDataConsumer.run(threadCount);

      final DataHandler monitorDataHandler = new MonitorDataHandler();

      final DataWorkerFactory monitorWorkerFactory
          = new DefaultWorkerFactory(monitorDataHandler,
              MonitorSubmissionData.class);

      final ConsumerConnector monitorKafkaConnector
          = createKafkaConsumer(clientId, zookeeperHost);
      final DataConsumer monitorDataConsumer = new DefaultConsumer(monitorTopic,
          monitorKafkaConnector,
          monitorWorkerFactory, executor);
      monitorDataConsumer.run(threadCount);

      LOG.info("Registering endpoints...");
      final SubmissionResource submissionResource
          = new SubmissionResource(submissionService);

      final InternalSubmissionResource internalSubmissionResource
          = new InternalSubmissionResource(submissionService);

      LOG.info("MDP server start has been successfully finished");

      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          executionDataConsumer.shutdown();
          compilationDataConsumer.shutdown();
          submissionProducer.shutdown();
        }
      });
    } catch (UnknownHostException ex) {
      LOG.error("Unable to determine local ip address", ex);
    }
  }

  /**
   * Creates Kafka connector instance.
   *
   * @param clientId      client id
   * @param zookeeperHost zookeeper host.
   * @return new created Kafka connector instance.
   */
  private static ConsumerConnector createKafkaConsumer(
      final String clientId, final String zookeeperHost) {
    Properties props = new Properties();
    props.put("zookeeper.connect", zookeeperHost);
    props.put("group.id", "mdp");
    props.put("client.id", clientId);
    props.put("zookeeper.session.timeout.ms", "400");
    props.put("zookeeper.sync.time.ms", "200");
    props.put("auto.commit.interval.ms", "1000");

    return Consumer.createJavaConsumerConnector(new ConsumerConfig(props));
  }
}
