
package cz.utb.fai.mdp.server.util;

import com.zaxxer.hikari.HikariConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.utils.ClassUtils;

/**
 *
 * @author expi
 */
public class ConfigurationUtil {

  private static final Logger LOG = LoggerFactory
      .getLogger(ConfigurationUtil.class);

  private static final String DEFAULT_CONFIG_FILE = "config.properties";

  private static Properties properties;

  public static void init() {
    try {
      InputStream configFile = ClassUtils
          .getDefaultClassLoader()
          .getResourceAsStream(DEFAULT_CONFIG_FILE);
      properties = new Properties();
      properties.load(configFile);
    } catch (IOException ex) {
      LOG.error("Unable to load properties from config file", ex);
    }
  }

  public static void initDebug(String path) {
    try {
      InputStream configFile = new FileInputStream(new File(path));
      properties = new Properties();
      properties.load(configFile);
    } catch (IOException ex) {
      LOG.error("Unable to load properties from config file", ex);
    }
  }

  public static String getProperty(String name) {
    return properties.getProperty(name);
  }

  public static Properties getProperties() {
    return properties;
  }

  public static HikariConfig dbConfig() {
    HikariConfig config = new HikariConfig();
    config.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
    config.setConnectionTestQuery("VALUES 1");
    config.addDataSourceProperty("URL", "jdbc:h2:mem:mdp");
    config.addDataSourceProperty("user", "sa");
    config.addDataSourceProperty("password", "sa");
    return config;
  }
}
