/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.kafka.impl;

import static cz.utb.fai.mdp.server.util.ConfigurationUtil.getProperty;

import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.server.kafka.SubmissionProducer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of {@link SubmissionProducer}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class DefaultSubmissionProducer implements SubmissionProducer {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(DefaultSubmissionProducer.class);

  /**
   * Kafka producer.
   */
  private final Producer producer;

  /**
   * Kafka full host with port number.
   */
  private String kafkaHost;

  /**
   * Topic for submission compilation.
   */
  private String compileTopic;

  /**
   * Topic for submission execution.
   */
  private String executeTopic;

  /**
   * Creates instance of default submission producer. This instances loads
   * configuration from property file. Configuration of properties can be over
   * written via {@link #setCompileTopic(java.lang.String) },
   * {@link #setExecuteTopic(java.lang.String)
   * } and {@link #setKafkaHost(java.lang.String) }.
   */
  public DefaultSubmissionProducer() {
    this.kafkaHost = getProperty("mdp.kafka.host");
    this.compileTopic = getProperty("mdp.kafka.compile.topic");
    this.executeTopic = getProperty("mdp.kafka.execute.topic");

    Map<String, String> properties = new HashMap<>();
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaHost);
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
        JsonMessageSerializer.class.getCanonicalName());
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
        "org.apache.kafka.common.serialization.StringSerializer");
    properties.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");
    properties.put(ProducerConfig.ACKS_CONFIG, "1");
    properties.put(ProducerConfig.RETRIES_CONFIG, "3");
    producer = new KafkaProducer(properties);
  }

  /**
   * Overrides default Kafka host from properties file.
   *
   * @param host full Kafka host with port number
   * @throws IllegalArgumentException if host is not non-empty string
   */
  public void setKafkaHost(final String host) {
    if (host == null || host.isEmpty()) {
      throw new IllegalArgumentException("host must be non-empty string");
    }
    this.kafkaHost = host;
  }

  /**
   * Overrides default compile topic from properties file.
   *
   * @param topic topic name to process compilation of submissions
   * @throws IllegalArgumentException if topic is not non-empty string
   */
  public void setCompileTopic(final String topic) {
    if (topic == null || topic.isEmpty()) {
      throw new IllegalArgumentException("CompileTopic must be non-empty "
          + "string");
    }
    this.compileTopic = topic;
  }

  /**
   * Overrides default execute topic from properties file.
   *
   * @param topic topic name to process execution of submissions
   * @throws IllegalArgumentException if topic is not non-empty string
   */
  public void setExecuteTopic(final String topic) {
    if (topic == null || topic.isEmpty()) {
      throw new IllegalArgumentException("topic must be non-empty "
          + "string");
    }
    this.executeTopic = topic;
  }

  @Override
  public void compileSubmission(final SubmissionData data) {
    produceMessage(compileTopic, data);
  }

  @Override
  public void executeSubmission(final SubmissionData data) {
    produceMessage(executeTopic, data);
  }

  @Override
  public void shutdown() {
    if (producer != null) {
      producer.close();
    }
  }

  /**
   * Produces message to Kafka log stream.
   *
   * @param topic destination of message
   * @param data  message data
   */
  private void produceMessage(final String topic, final SubmissionData data) {
    LOG.debug("Sending message {} to {}", data, topic);
    producer.send(new ProducerRecord(topic, data.getUuid(), data));
  }
}
