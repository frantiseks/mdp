/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.api;

import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.post;

import com.fasterxml.jackson.databind.ObjectMapper;

import cz.utb.fai.mdp.commons.enums.Language;
import cz.utb.fai.mdp.server.domain.Submission;
import cz.utb.fai.mdp.server.dto.CommandDto;
import cz.utb.fai.mdp.server.dto.SubmissionDto;
import cz.utb.fai.mdp.server.service.SubmissionService;
import cz.utb.fai.mdp.server.util.ConfigurationUtil;
import cz.utb.fai.mdp.server.util.UploadUtil;

import java.io.File;
import java.util.Optional;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class SubmissionResource {

  private static final String CONTEXT = "/mdp/api";
  private static final String OCTET_STREAM = "application/octet-stream";
  private static final String JSON_TYPE = "application/json";
  private final SubmissionService submissionService;

  public SubmissionResource(SubmissionService submissionService) {
    this.submissionService = submissionService;
    setupEndpoints();
  }

  private void setupEndpoints() {
    String uploadPath = ConfigurationUtil.getProperty("mdp.server.upload");

    post(CONTEXT + "/submissions", OCTET_STREAM, (request, response) -> {
      final byte[] zipFileBytes = request.bodyAsBytes();
      String lang = request.queryParams("lang");

      if (lang == null || lang.isEmpty() || zipFileBytes.length == 0) {
        halt(400);
      }

      File zipFile = UploadUtil
          .processZipUpload(zipFileBytes, uploadPath);
      String uuid = submissionService.create(zipFile, Language.fromCode(lang));

      if (uuid.isEmpty()) {
        halt(417);
      }

      submissionService.compile(uuid);
      response.type(JSON_TYPE);
      return new SubmissionDto(uuid);
    }, new JsonTransformer());

    post(CONTEXT + "/submissions/:uuid/execute", JSON_TYPE,
        (request, response) -> {
          String uuid = request.params(":uuid");

          Optional<Submission> opSubmission = submissionService.get(uuid);
          if (!opSubmission.isPresent()) {
            halt(404);
          }

          String requestBody = request.body();

          if (requestBody.isEmpty()) {
            halt(400);
          }

          CommandDto dto = new ObjectMapper().readValue(requestBody,
              CommandDto.class);

          submissionService.execute(uuid, dto.getArguments());

          return "";
        });

    get(CONTEXT + "/submissions/:uuid", (request, response) -> {
      String uuid = request.params(":uuid");

      Optional<Submission> opSubmission = submissionService.get(uuid);
      if (!opSubmission.isPresent()) {
        halt(404);
      }

      response.type(JSON_TYPE);
      return SubmissionDto.from(opSubmission.get());

    }, new JsonTransformer());
  }

}
