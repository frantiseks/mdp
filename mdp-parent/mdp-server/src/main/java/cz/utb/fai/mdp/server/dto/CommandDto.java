/*
 * 
 */

package cz.utb.fai.mdp.server.dto;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class CommandDto {

  private String arguments;

  public String getArguments() {
    return arguments;
  }

  public void setArguments(String arguments) {
    this.arguments = arguments;
  }

  @Override
  public String toString() {
    return "CommandDTO{" + "command=" + arguments + '}';
  }
}
