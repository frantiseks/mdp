/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.service.impl;

import cz.utb.fai.mdp.commons.dto.MonitorSubmissionData;
import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.server.service.DataHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class MonitorDataHandler implements DataHandler {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(MonitorDataHandler.class);

  @Override
  public final void handle(final SubmissionData data) {
    if (!(data instanceof MonitorSubmissionData)) {
      return;
    }

    MonitorSubmissionData monitorData = (MonitorSubmissionData) data;

    LOG.info("Output change for {} is {}",
        monitorData.getUuid(), monitorData.getOutputChange());

  }

}
