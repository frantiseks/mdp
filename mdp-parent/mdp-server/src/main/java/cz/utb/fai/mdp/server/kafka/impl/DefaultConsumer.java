/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.kafka.impl;

import cz.utb.fai.mdp.server.kafka.DataConsumer;
import cz.utb.fai.mdp.server.kafka.DataWorkerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Default implementation of {@link SubmissionConsumer}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class DefaultConsumer implements DataConsumer {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(DefaultConsumer.class);

  /**
   * Limit for waiting to thread termination.
   */
  private static final int AWAIT_MILLIS = 5000;

  /**
   * Topic which should be consumed by this consumer.
   */
  private final String topic;

  /**
   * Kafka consumer connector.
   */
  private final ConsumerConnector consumer;

  /**
   * Submissions processing factory.
   */
  private final DataWorkerFactory workerFactory;

  /**
   * Executor service for thread management.
   */
  private final ExecutorService executor;

  /**
   * Creates default consumer instance.
   *
   * @param consumerTopic     topic which should be consumed
   * @param consumerConnector Kafka consumer connector
   * @param factory           factory for processing message data
   * @param executorService   executor service instance
   */
  public DefaultConsumer(final String consumerTopic,
      final ConsumerConnector consumerConnector,
      final DataWorkerFactory factory,
      final ExecutorService executorService) {
    this.topic = consumerTopic;
    this.workerFactory = factory;
    this.consumer = consumerConnector;
    this.executor = executorService;
  }

  @Override
  public void run(final int threadCount) {
    Map<String, Integer> topicCountMap = new HashMap<>();
    topicCountMap.put(topic, threadCount);

    Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer
        .createMessageStreams(topicCountMap);

    List<KafkaStream<byte[], byte[]>> streams = consumerMap.get(topic);

    int threadNumber = 0;
    for (final KafkaStream stream : streams) {
      LOG.debug("Submitting thread number {} for stream with client id {}",
          threadNumber, stream.clientId());
      executor.submit(workerFactory.makeWorker(stream, threadNumber));
      threadNumber++;
    }
  }

  @Override
  public void shutdown() {
    if (consumer != null) {
      LOG.info("Closing consumer...");
      consumer.shutdown();
    }
    if (executor != null) {
      LOG.info("Closing thread executor...");
      executor.shutdown();
    }

    try {
      if (!executor.awaitTermination(AWAIT_MILLIS, TimeUnit.MILLISECONDS)) {
        LOG.warn("Timed out waiting for consumer threads to shut down, "
            + "exiting uncleanly");
      }
    } catch (InterruptedException ex) {
      LOG.error("Interrupted during shutdown, exiting uncleanly", ex);
    }
  }
}
