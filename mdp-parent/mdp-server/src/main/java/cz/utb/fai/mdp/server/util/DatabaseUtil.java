/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.mdp.server.util;

import com.zaxxer.hikari.HikariDataSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.utils.ResourceUtils;

import javax.sql.DataSource;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class DatabaseUtil {

  private static HikariDataSource datasource;
  private static final String SCHEME_FILE = "classpath:h2-ddl.sql";
  private static final Logger LOG = LoggerFactory
      .getLogger(DatabaseUtil.class);

  private DatabaseUtil() {

  }

  public static void init() {
    datasource = new HikariDataSource(ConfigurationUtil.dbConfig());
  }

  public static void createSchema() {
    createScheme(SCHEME_FILE);
  }

  public static void createScheme(String name) {
    try {
      File schemeFile = ResourceUtils.getFile(name);
      runScript(schemeFile);
    } catch (FileNotFoundException ex) {
      LOG.info("Scheme SQL file not found");
    } catch (SQLException ex) {
      LOG.error("SQL error");
    }
  }

  public static DataSource getDataSource() {
    return datasource;
  }

  public static void runScript(File file)
      throws FileNotFoundException, SQLException {
    Scanner scanner = new Scanner(file);

    List<String> queries = new ArrayList<>();
    while (scanner.hasNextLine()) {
      queries.add(scanner.nextLine());
    }

    try (Connection conn = datasource.getConnection()) {
      for (String q : queries) {
        PreparedStatement statement = conn.prepareStatement(q);
        statement.execute();
      }
    }
  }
}
