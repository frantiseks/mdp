/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.service;

import cz.utb.fai.mdp.commons.enums.Language;
import cz.utb.fai.mdp.server.domain.Submission;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public interface SubmissionService {

  /**
   *
   * @param zipFile
   * @param language
   * @return
   * @throws IOException
   */
  String create(File zipFile, Language language) throws IOException;

  /**
   *
   * @param uuid
   */
  void compile(String uuid);

  /**
   *
   * @param uuid
   * @return
   */
  Optional<Submission> get(String uuid);

  /**
   *
   * @param uuid
   * @param command
   */
  void execute(String uuid, String command);
}
