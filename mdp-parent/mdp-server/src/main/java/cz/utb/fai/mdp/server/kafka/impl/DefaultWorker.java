/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.kafka.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.mrbean.MrBeanModule;

import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.server.kafka.DataWorker;
import cz.utb.fai.mdp.server.service.DataHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.message.MessageAndMetadata;

import java.io.IOException;

/**
 *
 * Default implementation of {@link DataWorker}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class DefaultWorker implements DataWorker {

  /**
   * Logger instance for this class.
   */
  private static final Logger LOG = LoggerFactory
      .getLogger(DefaultWorker.class);

  private final KafkaStream messageStream;
  private final int threadNumber;
  private final DataHandler handler;
  private final ObjectMapper mapper;
  private final Class<? extends SubmissionData> deserializeClass;

  public DefaultWorker(
      final KafkaStream kafkaStream,
      final int workerId,
      final DataHandler dataHandler,
      final Class<? extends SubmissionData> clazz) {
    this.messageStream = kafkaStream;
    this.threadNumber = workerId;
    this.handler = dataHandler;

    this.mapper = new ObjectMapper();
    mapper.registerModule(new MrBeanModule());
    this.deserializeClass = clazz;
  }

  @Override
  public void run() {
    ConsumerIterator<String, byte[]> streamIt = messageStream.iterator();
    try {
      while (streamIt.hasNext()) {
        MessageAndMetadata message = streamIt.next();
        LOG.debug("Message with key {} is processed by thread {}",
            message.key(), threadNumber);

        SubmissionData data = mapper.readValue((byte[]) message.message(),
            deserializeClass);

        LOG.info("Processing submission data with uuid {}", data.getUuid());

        handler.handle(data);
      }
    } catch (IOException ex) {
      LOG.error("Unable to process message", ex);
    }
  }
}
