/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.service.impl;

import static cz.utb.fai.mdp.commons.util.WorkspaceUtil.workspace;
import static cz.utb.fai.mdp.commons.util.WorkspaceUtil.workspacePath;
import static java.lang.String.format;
import static org.apache.commons.io.FileUtils.copyFile;

import cz.utb.fai.mdp.commons.dto.SubmissionData;
import cz.utb.fai.mdp.commons.dto.impl.CompileSubmissionDataImpl;
import cz.utb.fai.mdp.commons.dto.impl.ExecuteSubmissionDataImpl;
import cz.utb.fai.mdp.commons.enums.Language;
import cz.utb.fai.mdp.server.dao.SubmissionDao;
import cz.utb.fai.mdp.server.domain.Submission;
import cz.utb.fai.mdp.server.exception.SubmissionNotFoundException;
import cz.utb.fai.mdp.server.kafka.SubmissionProducer;
import cz.utb.fai.mdp.server.service.SubmissionService;
import cz.utb.fai.mdp.server.util.ConfigurationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Optional;

/**
 * Implementation of {@link SubmissionService}.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class SubmissionServiceImpl implements SubmissionService {

  private static final Logger LOG = LoggerFactory
      .getLogger(SubmissionServiceImpl.class);

  private static final String BASE_INTERNAL_API
      = "http://%s/mdp/internal/submissions/%s/%s";

  private final SubmissionDao submissionDao;
  private final SubmissionProducer producer;
  private final String localhostWithPort;
  private String basePath;

  public SubmissionServiceImpl(SubmissionDao submissionDao,
      SubmissionProducer producer, String localhostWithPort) {
    this.submissionDao = submissionDao;
    this.producer = producer;
    this.localhostWithPort = localhostWithPort;
    this.basePath = ConfigurationUtil.getProperty("mdp.server.basePath");
  }

  public void setBasePath(final String basePath) {
    this.basePath = basePath;
  }

  @Override
  public String create(final File zipFile, final Language language)
      throws IOException {
    Submission submission = new Submission();
    submission.setLanguage(language.toString());
    String uuid = submissionDao.insert(submission);

    File workspace = workspace(basePath, uuid);
    if (!workspace.exists()) {
      workspace.mkdirs();
    }
    String zipFilename = workspacePath(basePath, uuid)
        + File.separator + uuid + ".zip";
    copyFile(zipFile, new File(zipFilename));

    return uuid;
  }

  @Override
  public void compile(final String uuid) {
    Optional<Submission> opSubmission = submissionDao.findOne(uuid);

    String downloadUrl = format(BASE_INTERNAL_API, localhostWithPort, uuid,
        "download");

    String uploadUrl = format(BASE_INTERNAL_API, localhostWithPort, uuid,
        "upload");

    Submission submission = opSubmission.orElseThrow(RuntimeException::new);
    Language lang = Language.fromCode(submission.getLanguage());

    try {
      SubmissionData data = new CompileSubmissionDataImpl(uuid,
          lang, downloadUrl, uploadUrl);
      producer.compileSubmission(data);
    } catch (MalformedURLException ex) {
      LOG.error("Invalid upload or download URL", ex);
    }

  }

  @Override
  public Optional<Submission> get(final String uuid) {
    return submissionDao.findOne(uuid);
  }

  @Override
  public void execute(final String uuid, final String arguments) {
    Optional<Submission> opSubmission = submissionDao.findOne(uuid);

    String binaryUrl = format(BASE_INTERNAL_API, localhostWithPort, uuid,
        "binary");

    Submission submission = opSubmission
        .orElseThrow(SubmissionNotFoundException::new);

    Language lang = Language.fromCode(submission.getLanguage());
    try {
      SubmissionData data = new ExecuteSubmissionDataImpl(uuid, lang, arguments,
          binaryUrl);
      producer.executeSubmission(data);
    } catch (MalformedURLException ex) {
      LOG.error("Invalid upload or download URL", ex);
    }
  }
}
