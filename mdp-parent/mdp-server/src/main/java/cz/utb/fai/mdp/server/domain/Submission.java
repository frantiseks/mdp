/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.domain;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class Submission {

  private long id;
  private String uuid;
  private int kafkaPartition;

  private String language;

  private String compilationOutput;
  private String executionOutput;

  private boolean compilationFailed;
  private boolean executionTerminated;
  private boolean alreadyCompiled;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public int getKafkaPartition() {
    return kafkaPartition;
  }

  public void setKafkaPartition(int kafkaPartition) {
    this.kafkaPartition = kafkaPartition;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getCompilationOutput() {
    return compilationOutput;
  }

  public void setCompilationOutput(String compilationOutput) {
    this.compilationOutput = compilationOutput;
  }

  public String getExecutionOutput() {
    return executionOutput;
  }

  public void setExecutionOutput(String executionOutput) {
    this.executionOutput = executionOutput;
  }

  public boolean isCompilationFailed() {
    return compilationFailed;
  }

  public void setCompilationFailed(boolean compilationFailed) {
    this.compilationFailed = compilationFailed;
  }

  public boolean isExecutionTerminated() {
    return executionTerminated;
  }

  public void setExecutionTerminated(boolean executionTerminated) {
    this.executionTerminated = executionTerminated;
  }

  public boolean isAlreadyCompiled() {
    return alreadyCompiled;
  }

  public void setAlreadyCompiled(boolean alreadyCompiled) {
    this.alreadyCompiled = alreadyCompiled;
  }

  @Override
  public String toString() {
    return "Submission{" + "uuid=" + uuid + '}';
  }

}
