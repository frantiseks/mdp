/*
 * Copyright (C) 2015 František Špaček <spacek at fai.utb.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.util;

import static cz.utb.fai.mdp.server.util.DirUtil.fixPath;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;

import cz.utb.fai.mdp.server.exception.InvalidSubmissionException;

import java.io.File;
import java.io.IOException;

/**
 * Utility class to process uploads of files.
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public final class UploadUtil {

  /**
   * This class should not be instantiated.
   */
  private UploadUtil() {

  }

  /**
   * Processes upload submission zip file.
   *
   * @param bytes      received bytes
   * @param uploadPath destination path of zip file
   * @return instance of File which points to created file on file system
   * @throws IOException                if error occurs during processing of
   *                                    upload
   * @throws InvalidSubmissionException if submission file is invalid
   */
  public static File processZipUpload(final byte[] bytes,
      final String uploadPath)
      throws IOException {
    boolean supported = ZipUtil.isZip(bytes);
    if (!supported) {
      throw new InvalidSubmissionException("Unsupported submission file");
    }

    return processFileUpload(bytes, uploadPath);
  }

  /**
   * Processes upload of generic file.
   *
   * @param bytes      received bytes
   * @param uploadPath destination path of upload file
   * @return instance of File which points to created file on file system
   * @throws IOException if error occurs during processing of upload
   */
  public static File processFileUpload(final byte[] bytes,
      final String uploadPath)
      throws IOException {
    File uploadedFile = new File(fixPath(uploadPath)
        + System.currentTimeMillis());
    writeByteArrayToFile(uploadedFile, bytes);

    return uploadedFile;
  }
}
