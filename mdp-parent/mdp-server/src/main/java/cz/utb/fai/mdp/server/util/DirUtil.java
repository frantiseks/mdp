/*
 * Copyright (C) 2015 František Špaček, FAI UTB Zlín
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.mdp.server.util;

import java.io.File;

/**
 *
 * @author František Špaček <spacek at fai.utb.cz>
 */
public class DirUtil {

  public static String fixPath(String path) {
    if (!path.endsWith("/")) {
      path += "/";
    }

    File pathFile = new File(path);
    if (!pathFile.exists()) {
      pathFile.mkdirs();
    }

    return path;
  }
}
